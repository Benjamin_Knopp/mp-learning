# -*- coding: utf-8 -*-
"""
Created on Fri Sep  3 14:44:19 2021

@author: damia
"""

vp_codes = [1234, 1313, 1735, 5724, 7127, 7575, 7660, 7811, 8638, 9436, 9933, 10438]
blocks = [1, 2, 3, 4]
for vp_code in vp_codes:
    for block in blocks:
        
        with open("data/"+str(vp_code)+"/block_"+str(block)+"_experiment_data.csv", 'r') as infile:
            for line in infile:
                line = line.replace("." , ",")
                outfile = open("data/"+str(vp_code)+"/block_"+str(block)+"_experiment_data_comma.csv", 'a')
                outfile.write(line)
        outfile.close()