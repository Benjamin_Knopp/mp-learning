#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Sep 26 19:18:28 2020

@author: dominik
"""

import torch,gpytorch
from gpytorch.lazy.non_lazy_tensor import NonLazyTensor



def batch_cholesky_inverse(batch_mat):
    orig_shape=batch_mat.shape
    batch_mat=batch_mat.flatten(end_dim=-3)
    boost_val=1e-3 * batch_mat.abs().max()
    for b in range(20): #6
        try:
            return torch.cat([torch.cholesky_inverse(x) for x in torch.cholesky(batch_mat)]).reshape(orig_shape)
        except RuntimeError: # singular matrix, boost diagonals
            batch_mat = batch_mat + torch.eye(batch_mat.shape[-1])*boost_val
            boost_val*=10.0
    
    print(batch_mat)
    raise RuntimeError("batch_cholesky_inverse: could not find approximate inverse after boosting")
        
    

class WeightedMarginalLogLikelihood(gpytorch.mlls.marginal_log_likelihood.MarginalLogLikelihood):
    r"""
    An approximate marginal log likelihood (typically a bound) for approximate GP models.
    Log-likelihood term can be weighted per datapoint, which is useful for hierarchical models
    
    We expect that :attr:`model` is a :obj:`gpytorch.models.ApproximateGP`.

    Args:
        :attr:`likelihood` (:obj:`gpytorch.likelihoods.Likelihood`):
            The likelihood for the model
        :attr:`model` (:obj:`gpytorch.models.ApproximateGP`):
            The approximate GP model
        :attr:`combine_terms` (bool):
            Whether or not to sum the expected NLL with the KL terms (default True)
    """

    def __init__(self, likelihood, model, combine_terms=True):
        super().__init__(likelihood, model)
        self.combine_terms = combine_terms
        self._KL_weight=1.0
        
    @property
    def KL_weight(self):
        return self._KL_weight
    
    @KL_weight.setter
    def KL_weight(self,val):
        if val<=0:
            raise ValueError("KL-divergence weight must be positive, but got",val)
        self._KL_weight=val
        
        
    def _log_likelihood_term(self, approximate_dist_f, target, **kwargs):
        if "logl_weights" in kwargs: # in this case, there must be a weight tensor rather than a weight list
            segment_weights=kwargs.pop("logl_weights")
        else:
            segment_weights=1.0
        llhood=segment_weights*self.likelihood.expected_log_prob(target, approximate_dist_f, **kwargs)
        return llhood.sum(-1)


    def forward(self, approximate_dist_f, target, **kwargs):
        r"""
        Computes the Variational ELBO given :math:`q(\mathbf f)` and `\mathbf y`.
        Calling this function will call the likelihood's `expected_log_prob` function.
        target and approximate_dist_f may be lists of predictions and targets, respectively.

        Args:
            :attr:`approximate_dist_f` (:obj:`gpytorch.distributions.MultivariateNormal`):
                :math:`q(\mathbf f)` the outputs of the latent function (the :obj:`gpytorch.models.ApproximateGP`), may be a list of such outputs
            :attr:`target` (`torch.Tensor`):
                :math:`\mathbf y` The target values, may be a list of tensors
            :attr:`**kwargs`:
                Additional arguments passed to the likelihood's `expected_log_prob` function.
                if these arguments contain a keyword 'logl_weights', then it must be a list of multiplicative weigths
                ,one for each target. 
        """
        # Get likelihood term and KL term
        if type(approximate_dist_f)==list and type(target)==list:
            num_current=sum([tt.shape[-1] for tt in target]) # datapoints in last dim
            if "logl_weights" in kwargs.keys():
                weights=kwargs.pop("logl_weights")
                log_likelihood=sum([w*self._log_likelihood_term(adf, tt.T, **kwargs) for adf,tt,w in zip(approximate_dist_f, target, weights)])
            else:
                log_likelihood=sum([self._log_likelihood_term(adf, tt.T, **kwargs) for adf,tt in zip(approximate_dist_f, target)])
        else:
            log_likelihood = self._log_likelihood_term(approximate_dist_f, target.T, **kwargs)
            
        kl_w = self._KL_weight
        
        kl_divergence = self.model.variational_strategy.kl_divergence().sum()*self._KL_weight

        # Add any additional registered loss terms
        added_loss = torch.zeros_like(log_likelihood)
        had_added_losses = False
        for added_loss_term in self.model.added_loss_terms():
            added_loss.add_(added_loss_term.loss())
            had_added_losses = True

        # Log prior term
        log_prior = torch.zeros_like(log_likelihood)
        for full_name, module, prior, closure, inv_closure in self.named_priors():
            if 'noise_prior' in full_name:
                log_prior.add_(prior.log_prob(closure(module)).sum()) #.div(self.num_data))
            else:
                log_prior.add_(prior.log_prob(closure()).sum()) #.div(self.num_data))
            

        if self.combine_terms:
            return log_likelihood - kl_divergence + log_prior - added_loss
        else:
            if had_added_losses:
                return log_likelihood, kl_divergence, log_prior.div(self.num_data), added_loss
            else:
                return log_likelihood, kl_divergence, log_prior.div(self.num_data)
            
            
            
# softplus/inverse softplus versions that can be pickled. Workaround for the torch bug
def picklable_softplus(x,beta=1,threshold=20):
    bx = beta * x
    y=x.clone()
    y[bx <= threshold] = 1 / beta * torch.log(1 + torch.exp(bx[bx <= threshold]))
    return y

def picklable_inv_softplus(x,beta=1,threshold=20):
    bx = beta*x
    y=x.clone()
    y[bx <= threshold] = 1/beta * torch.log(torch.exp(bx[bx <= threshold])-1.0)
    return y

class DiagonalGreaterThan(gpytorch.constraints.GreaterThan):
    """Constraint to enforce diagonal entries of a matrix > some lower bound. Works also for batches of matrices"""
    
    def __init__(self, lower_bound, transform=picklable_softplus, inv_transform=picklable_inv_softplus, initial_value=None):
        super().__init__(lower_bound, transform, inv_transform, initial_value)
    
    
    def transform(self, matrix):
        if not self.enforced:
            return matrix
        
        diag_range=range(matrix.shape[-1])
        transformed_matrix=matrix.clone()
        transformed_matrix[...,diag_range,diag_range]=self._transform(matrix[...,diag_range,diag_range])
        
        return transformed_matrix

    def inverse_transform(self, transformed_matrix):
        if not self.enforced:
            return transformed_matrix
        
        diag_range=range(transformed_matrix.shape[-1])
        matrix=transformed_matrix.clone()
        matrix[...,diag_range,diag_range]=self._inv_transform(transformed_matrix[...,diag_range,diag_range])
    
        return matrix
    
   
    def check(self, tensor):
        diag_range=range(tensor.shape[-1])
        return bool(torch.all(tensor[...,diag_range,diag_range] <= self.upper_bound))

    def check_raw(self, tensor):
        diag_range=range(tensor.shape[-1])
        return bool(torch.all(self.transform(tensor)[...,diag_range,diag_range] <= self.upper_bound))
    
    
class VariationalStrategyWithPosteriorInit(gpytorch.variational.VariationalStrategy):
    
    def initialize_variational_distribution(self,posterior):
        """Initialize variational posterior (inducing value distribution) to the mean and covariance of the supplied posterior"""
        with torch.no_grad():
            self._variational_distribution.initialize_variational_distribution(gpytorch.distributions.MultivariateNormal(posterior.mean,posterior.covariance_matrix))
            self.variational_params_initialized.fill_(1)
    
    
