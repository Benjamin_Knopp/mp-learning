﻿import viz
import viztask
import vizshape
import vizproximity
import vizact
from math import cos, sin

height = 1.5

class BodyPart:
	def __init__(self, name, position, parent):
		"""
		Diese Klasse erstellt Körperteile bestehend
		aus einem Gelenk und einem Knochen, und einem
		Elternteil.
		"""
		self.name = name
		# das Gelenk wird durch eine Kugel visualisiert
		self.joint = vizshape.addSphere(radius=0.05)
		# hier wird das Elternteil festgelegt (viz.WORLD für Anfangsgelenk)
		self.joint.setParent(parent)
		# hier wird die Position relativ zum Parent festgelegt
		self.joint.setPosition(position)

		# der Knochen ist ein Zylinder
		self.bone = vizshape.addCylinder(armlength, 0.03)
		# Parent ist das Gelenk. Dadurch bewegt sich der Knochen immer mit dem Gelenk mit
		self.bone.setParent(self.joint)
		# da der Zylinder den Ursprung des Koordinatensystems in der Mitte hat, muss man ihn verschieben.
		self.bone.setPosition(0, armlength/2.0)


## Den künstlichen Arm erstellen:
armlength = 0.36  # Durchschnitts(ober)arm ist 36cm lang
# Oberarm ist Startpunkt der Kette
upper = BodyPart("Oberarm", [0, height, 0.75], viz.WORLD)
# dessen Gelenk wird 90° um die x-Achse gedreht damit der Knochen horizontal ist
upper.joint.setAxisAngle(1, 0, 0, 90)
# der Unterarm hängt am Oberarm mit dem Abstand einer Armlänge
lower = BodyPart("Unterarm",[0, armlength, 0], upper.joint)
# die Hand wird jetzt an den Unterarm gehängt.
# Wir machen das ohne die Klasse `BodyPart`, da unsere Hand keinen Knochen hat
hand = vizshape.addSphere(radius=0.05,color=viz.YELLOW)
hand.setParent(lower.joint)
hand.setPosition(0, armlength)


def rotate(obj, inc=-5):
	"""
	Diese Funktion wird in der Desktop Version des Experimentes genutzt um
	den Arm zu kontrollieren.
	`obj` ist hier ein Gelenk der Schulter, oder des Ellenbogens.
	"""
	a, b, c = obj.getEuler()
	obj.setEuler(a, b, c+inc)
	
	
def rotate_up(obj, inc=-5):
	"""
	Diese Funktion wird in der Desktop Version des Experimentes genutzt um
	den Arm zu kontrollieren.
	`obj` ist hier ein Gelenk der Schulter, oder des Ellenbogens.
	"""
	a, b, c = obj.getEuler()
	obj.setEuler(a, b+inc, c)

			#self.sphere.alpha(1.0)
	

# Der Event-Manager beobachtet und organisiert Ereignisse.
#manager.setDebug(viz.ON)

# Steuerung des Ellenbogengelenks
vizact.onkeydown('k', rotate, lower.joint, -5)
vizact.onkeydown('j', rotate, lower.joint, 5)
# Steuerung des Schultergelenks
vizact.onkeydown('d', rotate, upper.joint, -5)
vizact.onkeydown('a', rotate, upper.joint, 5)
#Steuerung der Auf- und Abbewegung des Schultergelenks
vizact.onkeydown('w', rotate_up, upper.joint, -5)
vizact.onkeydown('s', rotate_up, upper.joint, 5)




# Start und Zielball werden erstellt. Der name der
# Objekte wurde so gewählt, um den Versuchspersonen
# grammatikalisch korrekte Anweisungen zu geben.
#left_sphere = Sphere(viz.RED, "linken", manager)
#right_sphere = Sphere(viz.GREEN, "rechten", manager)

#positions = [
#[0.3, 1.5, 1.4],
#[-0.3, 1.5, 1.4],
#[0.3, 1.8, 1.4],
#[-0.3, 1.8, 1.4],
#[0.0, 2.0, 1.0]
#]

#colors = [viz.BLUE, viz.GREEN, viz.YELLOW, viz.VIOLET, viz.ORANGE]

#for (c, p) in zip(colors, positions):
	#spheres = [Sphere(c, p, manager)]
#start_sphere = Sphere(viz.RED, (0.0, 1.4, 1.4), manager)
#start_sphere.sphere.alpha(1.0)

# Hier wird das Hindernis erstellt:
#obstacle = vizshape.addCylinder(0.5, 0.01)
#obstacle.setPosition(0, 1.5, 0.75 + 2*armlength - 0.15)
#ceil = vizshape.addCylinder(0.5, 0.01)
#ceil.setPosition(0, 1.75, 0.5 + 2*armlength - 0.15)
#ceil.setAxisAngle(1, 0, 0, 90)
#floor = vizshape.addCylinder(0.5, 0.01)
#floor.setPosition(0, 1.25, 0.5 + 2*armlength - 0.15)
#floor.setAxisAngle(1, 0, 0, 90)

# Hier wird der Sensorbereich für das Hindernis festgelegt.
# Da unser Target die Hand ist, wäre es möglich diese hinter
# dem Hindernis herzuführen. Diese Verhalten ist nicht erwünscht,
# daher wird hier eine dünne Wand erstellt, ausgeschnitten in dem
# Bereich zwischen Hindernis und Schulter.
#group = viz.addGroup()
#box_front = vizproximity.Box([0.01, 0.5, 1], center=[0, 1.5, 1.82])
#box_back = vizproximity.Box([0.01, 0.5, 1], center=[0, 1.5, 0])
#box_ceil = vizproximity.Box([0.01, 0.5, 5], center=[0, 2, .75])
#box_floor = vizproximity.Box([0.01, 0.5, 5], center=[0, 1, .75])
#composite = vizproximity.CompositeShape([box_front, box_back, box_ceil, box_floor])
#obstacle_sensor = vizproximity.Sensor(composite, group)
#manager.addSensor(obstacle_sensor)