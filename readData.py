# -*- coding: utf-8 -*-
"""
Created on Tue Jul  6 14:19:55 2021

@author: damia
"""

import pandas as pd
import numpy as np
import transforms3d


def convert_to_axis_angle(x_euler, axes="syzx"):
    x_euler = x_euler.copy()
    x_euler *= np.pi / 180  # conversion from grad to rad
    assert (
        np.abs(x_euler).max() < np.pi
    )  # checks condition if true:contrinue, otherwise false
    T, dim = x_euler.shape  # T: number of data points of joints, dim: joint angles
    new_dim = dim * 4 // 3
    x_transform = np.nan * np.empty((T, new_dim))
    i = 0
    theta_list = []
    for x_row in x_euler:  # x_row represents joint information related to one datapoint
        a = x_row[0::3]  # taking every third element starting from 0
        b = x_row[1::3]  # taking every third element starting from 1
        c = x_row[2::3]  # taking every third element starting from 2
        tmp = np.empty((new_dim,))  # random 88 numbers in tmp[]
        j = 0
        for ai, aj, ak in zip(
            a, b, c
        ):  # concatenate a,b,c and calculates angles to each other (x-y, x-z, y-z)
            unit_vector, theta = transforms3d.euler.euler2axangle(
                ai,
                aj,
                ak,  # ai: rotation angle on y-axis, aj: rotation angle on z-axis, ak: rotation angle on y-axis
                axes=axes,
            )
            theta_list.append(theta)

            assert np.allclose(
                np.linalg.norm(unit_vector), 1
            )  # check if unit_vector is correct
            tmp[j : j + 4] = np.array(
                [theta, *unit_vector]
            )  # storage of theta and unit_vector in tmp (all joints from one datapoint)
            j += 4  # iterate over every euler angles of each joints
        x_transform[i] = np.array(
            tmp
        )  # storage tmp in x_transform so that x_transform has all joint information of all datapoints
        i += 1
    return x_transform


def read_data(vp, block):
    fn = f'data/{vp}/block_{block}_experiment_data.csv'
    with open(fn) as df:
        raw_data = df.readlines()

    D = pd.DataFrame([line.replace('\t\t\t', '\t').replace('\t\t', '\t').replace('\n', '').split('\t') for line in open(fn)][3:])
    D.columns = ['trial', 'condition', 'time']
    D.loc[:, 'time'] = D.loc[:, 'time'].apply(float)

    data_list = []
    trials = range(50)
    for i in trials:
        with open(
            f"data/{vp}/block_{block}_controller_tracking_trial_{i}_of_50.txt", "r"
        ) as df:
            data = df.readlines()
            X = []
            for x_t_str in data:
                x_t = [float(x) for x in x_t_str.split("\t")]
                X.append(x_t)
            m = np.array(X)
        data_list.append(m)

    axis_angle_list = []
    for k in data_list:
        axis_angles_joints = convert_to_axis_angle(k)
        x_deg = axis_angles_joints[
            :, ::4
        ].T  # extracting all theta angles of all datapoints, then transpose so that row: joints and column: datapoionts (JxT)
        # x_expmap [TxJ] in format: [T[1]: x_deg*x_axis [0:21], x_deg*y_axis [22:43], x_deg*z_axis[44:65]]
        x_expmap = np.vstack(
            (
                x_deg * axis_angles_joints[:, 1::4].T,
                x_deg * axis_angles_joints[:, 2::4].T,
                x_deg * axis_angles_joints[:, 3::4].T,
            )
        ).T

        arr = np.array(x_expmap)  # arr: axisangle in exponentialmap (TxJ)
        array = arr.T
        axis_angle_list.append(array)

    return axis_angle_list, D


data, meta = read_data(1234, 2)
# print(data)
