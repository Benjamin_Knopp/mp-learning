﻿import viz
import viztask
import vizshape
import vizproximity
import vizact
import arm
from math import cos, sin

height = arm.height

def polar(theta, phi, r=0.5, origin=[0.0, height, 0.75]):
    x, y, z = origin
    return [r * sin(theta) * cos(phi) + x, r * sin(theta) * sin(phi) + y, r * cos(theta)+ z]
class Sphere:
    def __init__(self, color, position, radius, l, manager):
        """
        Diese Klasse wird benutzt um Start und Zielball zu erstellen.
        Die Bälle unterscheiden sich in Farbe und Position.
        Die Bälle besitzen Sensoren, die bei Erstellung automatisch
        dem Event-Manager mitgeteilt werden.
        """
        self.sphere = viz.addChild("beachball.osgb", scale=(0.3, 0.3, 0.3))
        self.sphere.color(color)
        #self.sphere.alpha(0.0)
        self.position = position
        #self.sphere.setPosition(position)
#        if position == "rechten":
#            self.sphere.setPosition([0.3, 1.5, 1.4])
#        elif position == "linken":
#            self.sphere.setPosition([-0.3, 1.5, 1.4])
        if position == "rechten":
            #[0.2, 1.5, 1.4]
            position = polar(l[5],l[4], r=radius)
            print("Position rechts", position)
            self.sphere.setPosition(position)
        elif position == "linken":
            position = polar(l[2],l[4], r=radius)
            print("Position links", position)
            self.sphere.setPosition(position)
        elif position == "oberen linken":
            position = polar(l[5],l[7], r=radius)
            print("Position oben links", position)
            self.sphere.setPosition(position)
        elif position == "oberen rechten":
            position = polar(l[2],l[0], r=radius)
            print("Position oben rechts", position)
            self.sphere.setPosition(position)
        elif position == "hinteren":
            position = polar(l[3],l[3], r=radius)
            print("Position hinten", position)
            self.sphere.setPosition(position)
        elif position == "unteren rechten":
            position = polar(l[5],l[1], r=radius)
            print("Position unten rechts", position)
            self.sphere.setPosition(position)
        elif position == "unteren linken":
            position = polar(l[2],l[6], r=radius)
            print("Position unten links", position)
            self.sphere.setPosition(position)
        elif position == "mittleren":
            position = [0.0, height, 1.0]
            print("Position mitte", position)
            self.sphere.setPosition(position)
        
        #self.position = position
        # hier wird ein Sensorbereich um den Ball erstellt
        self.sensor = vizproximity.addBoundingSphereSensor(self.sphere, scale=0.6)
        # dem Manager wird mitgeteilt, falls ein Target (in unserem Fall die Hand)
        # sich im Sensorbereich aufhält
        manager.addSensor(self.sensor)
        # Methode _enter_sphere wird aufgerufen sobald die Hand in den Bereich kommt
        #manager.onEnter(self.sensor, self._enter_sphere)
        # Methode _exit_sphere wird aufgerufen sobald die Hand den Bereich verlässt
        #manager.onExit(self.sensor, self._exit_sphere)
    def _show_sphere(self):
        self.sphere.alpha(1.0)
    
    def _hide_sphere(self):
        self.sphere.alpha(0.0)
                
    def _destroy_sphere(self):
        self.sphere.remove()
    
    def _enter_sphere(self, e):
        #mid_sphere.sphere.alpha(0.5)
        """
        Der Ball wird transparent sobald die Hand im Ball ist.
        Der Manager gibt der Funktion Event-Objekt `e` mit, welches
        Information z.B. über den ausgelösten Sensor enthält.
        """
        pass
        
    def _exit_sphere(self, e):
        #mid_sphere.sphere.alpha(1)
        """
        Der Ball wird wieder undurchlässig sobald die Hand den Ball verlässt.
        """
        pass
        
manager = vizproximity.Manager()
# Die Hand wird zu dem Target erklärt...
target = vizproximity.Target(arm.hand)
# ...auf welches der Manager aufpasst, d.h.
# der Manager überwacht ob die Hand in einem der von ihm verwalteten Sensoren ist.
manager.addTarget(target)
def _show_mid_sphere(self):
    self.sphere.alpha(1.0)
    
def disable_transparency(e,sphere):
    sphere.sphere.alpha(1)
    
    
def enable_transparency(e,sphere):
    sphere.sphere.alpha(0.5)
def create_spheres(radius, l):
    right_sphere = Sphere(viz.GREEN, "rechten", radius, l, manager)
    left_sphere = Sphere(viz.GREEN, "linken", radius, l, manager)
    upper_left_sphere = Sphere(viz.GREEN, "oberen linken", radius, l, manager)
    upper_right_sphere = Sphere(viz.GREEN, "oberen rechten", radius, l, manager)
    rear_sphere = Sphere(viz.GREEN, "hinteren", radius, l, manager)
    mid_sphere = Sphere(viz.RED, "mittleren", radius, l, manager)
    _show_mid_sphere(mid_sphere)
    manager.onExit(mid_sphere.sensor, disable_transparency, mid_sphere)
    manager.onEnter(mid_sphere.sensor, enable_transparency, mid_sphere)
    lower_right_sphere = Sphere(viz.GREEN, "unteren rechten", radius, l, manager)
    lower_left_sphere = Sphere(viz.GREEN, "unteren linken", radius, l, manager)
    spheres = {
        0: right_sphere,
        1: left_sphere,
        2: upper_left_sphere,
        3: upper_right_sphere,
        4: rear_sphere,
        5: lower_right_sphere,
        6: lower_left_sphere,
        "start_sphere": mid_sphere}
    return spheres