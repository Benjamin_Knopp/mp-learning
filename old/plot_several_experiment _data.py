# -*- coding: utf-8 -*-
"""
Created on Fri Sep 11 16:44:18 2020

@author: damia
"""

import numpy as np
import matplotlib.pyplot as plt

def plot_all_experiment_data(participant):
    
    #open data set
    with open('data/' + participant + '/block_1_experiment_data.csv', 'r') as df:
        data = df.readlines()

    #creating list of floats
    X = []
    for x_t_str in data [3:]:
        x_t = [float(x) for x in x_t_str.split('\t\t')]
        X.append(x_t)
    
    #creating lists for trials, condition and time   
    trials = [item[0] for item in X]
    condition = [item[1] for item in X]
    time = [item[2] for item in X]
    
    #creating lists for time depending on condition
    #and printing out exact mean_time for every condition
    indices_0 = [i for i, k in enumerate(condition) if k == 0]
    indices_1 = [i for i, k in enumerate(condition) if k == 1]
    indices_2 = [i for i, k in enumerate(condition) if k == 2]
    indices_3 = [i for i, k in enumerate(condition) if k == 3]
    indices_4 = [i for i, k in enumerate(condition) if k == 4]
    indices_5 = [i for i, k in enumerate(condition) if k == 5]
    indices_6 = [i for i, k in enumerate(condition) if k == 6]
    time_0 = [time[i] for i in indices_0]
    time_1 = [time[i] for i in indices_1]
    time_2 = [time[i] for i in indices_2]
    time_3 = [time[i] for i in indices_3]
    time_4 = [time[i] for i in indices_4]
    time_5 = [time[i] for i in indices_5]
    time_6 = [time[i] for i in indices_6]
    print("The mean time for condition 0 is:", np.mean(time_0),
          "\nThe mean time for condition 1 is:", np.mean(time_1),
          "\nThe mean time for condition 2 is:", np.mean(time_2),
          "\nThe mean time for condition 3 is:", np.mean(time_3),
          "\nThe mean time for condition 4 is:", np.mean(time_4),
          "\nThe mean time for condition 5 is:", np.mean(time_5),
          "\nThe mean time for condition 6 is:", np.mean(time_6))
    condition_means = [np.mean(time_0), np.mean(time_1), np.mean(time_2), np.mean(time_3), np.mean(time_4), np.mean(time_5), np.mean(time_6)]
    return time, trials, condition_means
    #creating list of time means
    #(every value of the list is the average time of its condition)
    time_means = list.copy(time)
    for n, i in enumerate(time_means):
        if time_means.index(i) in indices_0:
            time_means[n] = np.mean(time_0) 
    for n, i in enumerate(time_means):
        if time_means.index(i) in indices_1:
            time_means[n] = np.mean(time_1)
    for n, i in enumerate(time_means):
        if time_means.index(i) in indices_2:
            time_means[n] = np.mean(time_2)
    for n, i in enumerate(time_means):
        if time_means.index(i) in indices_3:
            time_means[n] = np.mean(time_3)
    for n, i in enumerate(time_means):
        if time_means.index(i) in indices_4:
            time_means[n] = np.mean(time_4)
    for n, i in enumerate(time_means):
        if time_means.index(i) in indices_5:
            time_means[n] = np.mean(time_5)
    for n, i in enumerate(time_means):
        if time_means.index(i) in indices_6:
            time_means[n] = np.mean(time_6)
    
    #plotting time per trial
    plt.plot(trials, time)
    plt.title('time per trial')
    plt.xlabel('trial')
    plt.ylabel('time (s)')
    plt.grid()
    plt.show()
    
    #plotting average time per condition
    plt.bar(condition, time_means)
    plt.title('time per condition')
    plt.xlabel('condition')
    plt.ylabel('time (s)')
    plt.grid()
    plt.show()
    
    
participants = ['Selbsttest_1', 'Selbsttest_2']
mean_times = []
all_condition_means = []
conditions = [0, 1, 2, 3, 4, 5, 6]

for participant in participants:
    time, trials, condition_means = plot_all_experiment_data(participant)
    mean_times.append(time)
    all_condition_means.append(condition_means)
    
arrays = [np.array(x) for x in mean_times]
whole_mean_time = [np.mean(k) for k in zip(*arrays)]

arrays = [np.array(x) for x in all_condition_means]
whole_condition_means = [np.mean(k) for k in zip(*arrays)]

plt.plot(trials, whole_mean_time)
plt.title('time per trial')
plt.xlabel('trial')
plt.ylabel('time (s)')
plt.grid()
plt.show()

plt.bar(conditions, whole_condition_means)
plt.title('time per condition')
plt.xlabel('condition')
plt.ylabel('time (s)')
plt.grid()
plt.show()
