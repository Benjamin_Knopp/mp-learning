# -*- coding: utf-8 -*-
"""
Created on Thu Sep  2 15:00:59 2021

@author: damia
"""

import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

vp_codes = [1234, 1313, 1735, 5724, 7127, 7575, 7660, 7811, 8638, 9436, 9933, 10438]
for vp_code in vp_codes:
    block = 1
    fig, ax = plt.subplots(ncols=4, sharey=True)
    fig.set_size_inches(20, 5)

    for block in range(1, 5):
        fn = 'data/{vp_code}/block_{block}_experiment_data.csv'
        with open(fn) as df:
            raw_data = df.readlines()

        D = pd.DataFrame([line.replace('\t\t\t', '\t').replace('\t\t', '\t').replace('\n', '').split('\t') for line in open(fn)][3:])
        D.columns = ['trial', 'condition', 'time']
        print ("Mean =" + str(np.mean(D)))


        D.loc[:, 'time'] = D.loc[:, 'time'].apply(float)
        D.loc[:, 'trial'] = D.loc[:, 'trial'].apply(int)
        for cond, d in D.groupby('condition'):
            ax[block-1].plot(d.trial, d.time, 'x', label=cond)
        ax[block-1].legend()
        # ax[block-1].plot(D.trial, D.time, 'x')