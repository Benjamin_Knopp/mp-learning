# -*- coding: utf-8 -*-
"""
Created on Fri Sep 11 16:44:18 2020

@author: damia
"""

import numpy as np
import matplotlib.pyplot as plt

#open data set
with open('data/1234/block_4_experiment_data.csv', 'r') as df:
    data = df.readlines()

#removing the missed trials
data_clean = data.copy()
data_clean = [x for x in data if not 'miss' in x]

#creating list of floats
X = []
for x_t_str in data_clean [3:]:
    x_t = [float(x) for x in x_t_str.split('\t\t')]
    X.append(x_t)

#creating lists for trials, condition and time   
trials = [item[0] for item in X]
condition = [item[1] for item in X]
time = [item[2] for item in X]

print("Mean time overall is:", np.mean(time))

#creating lists for time depending on condition
#and printing out exact mean_time for every condition
indices_0 = [i for i, k in enumerate(condition) if k == 0]
indices_1 = [i for i, k in enumerate(condition) if k == 1]
indices_2 = [i for i, k in enumerate(condition) if k == 2]
indices_3 = [i for i, k in enumerate(condition) if k == 3]
indices_4 = [i for i, k in enumerate(condition) if k == 4]
indices_5 = [i for i, k in enumerate(condition) if k == 5]
indices_6 = [i for i, k in enumerate(condition) if k == 6]
time_0 = [time[i] for i in indices_0]
time_1 = [time[i] for i in indices_1]
time_2 = [time[i] for i in indices_2]
time_3 = [time[i] for i in indices_3]
time_4 = [time[i] for i in indices_4]
time_5 = [time[i] for i in indices_5]
time_6 = [time[i] for i in indices_6]
print("The mean time for condition 0 is:", np.mean(time_0),
      "\nThe mean time for condition 1 is:", np.mean(time_1),
      "\nThe mean time for condition 2 is:", np.mean(time_2),
      "\nThe mean time for condition 3 is:", np.mean(time_3),
      "\nThe mean time for condition 4 is:", np.mean(time_4),
      "\nThe mean time for condition 5 is:", np.mean(time_5),
      "\nThe mean time for condition 6 is:", np.mean(time_6))

#creating list of time means
#(every value of the list is the average time of its condition)
time_means = list.copy(time)
for n, i in enumerate(time_means):
    if time_means.index(i) in indices_0:
        time_means[n] = np.mean(time_0) 
for n, i in enumerate(time_means):
    if time_means.index(i) in indices_1:
        time_means[n] = np.mean(time_1)
for n, i in enumerate(time_means):
    if time_means.index(i) in indices_2:
        time_means[n] = np.mean(time_2)
for n, i in enumerate(time_means):
    if time_means.index(i) in indices_3:
        time_means[n] = np.mean(time_3)
for n, i in enumerate(time_means):
    if time_means.index(i) in indices_4:
        time_means[n] = np.mean(time_4)
for n, i in enumerate(time_means):
    if time_means.index(i) in indices_5:
        time_means[n] = np.mean(time_5)
for n, i in enumerate(time_means):
    if time_means.index(i) in indices_6:
        time_means[n] = np.mean(time_6)

#plotting time per trial
plt.plot(trials, time)
plt.title('time per trial')
plt.xlabel('trial')
plt.ylabel('time (s)')
plt.grid()
plt.show()

#plotting average time per condition
plt.bar(condition, time_means)
plt.title('time per condition')
plt.xlabel('condition')
plt.ylabel('time (s)')
plt.grid()
plt.show()

x_data = np.array(trials)
y_data = np.array(time)

log_x_data = np.log(x_data)
log_y_data = np.log(y_data)

curve_fit = np.polyfit(x_data, log_y_data, 2)
print(curve_fit)

y = np.exp(curve_fit[0]) * np.exp(curve_fit[1]*x_data)
plt.plot(x_data, y_data, "o")
plt.plot(x_data, y)