# -*- coding: utf-8 -*-
"""
Created on Mon May 31 15:42:12 2021

@author: damia
"""

import numpy as np
import matplotlib.pyplot as plt

with open('data/Damian/block_1_tracking_trial_0_of_10_condition_3.txt', 'r') as df:
    data = df.readlines()

X = []
for x_t_str in data:
    x_t = [float(x) for x in x_t_str.split('\t')]
    X.append(x_t)
    
shoulder_tracking = [item[0:3] for item in X]
elbow_tracking = [item[3:6] for item in X]

#plotting shoulder trajectories
plt.plot(shoulder_tracking)
plt.title('Euler angles for shoulder movement')
plt.xlabel('time (s/10)')
plt.ylabel('Euler angle (yaw=blue, pitch=yellow, roll=green)')
plt.grid()
plt.show()

#plotting elbow trajectories
plt.plot(elbow_tracking)
plt.title('Euler angles for shoulder movement')
plt.xlabel('time(s/10)')
plt.ylabel('Euler angle (yaw, blue, pitch=yellow, roll=green)')
plt.grid()
plt.show()