#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Sep  2 09:26:23 2020

@author: dom
edited by Anja

Free-energy based version of the Temporal movement primitive model.
Can run MP learning on the GPU, which is about 6x faster than CPU on a GTX 960.
Also supports minibatch learning if not all of the data fit on the GPU at once.

"""


import time, itertools, unittest

# from functools import cached_property

import numpy as np
import scipy.signal
import matplotlib

matplotlib.use("Agg")
import matplotlib.pyplot as plt


import WeightedMarginalLogLikelihood as wmll
import torch, gpytorch

use_cuda = torch.cuda.is_available()  #  and False


class TMPModel(gpytorch.models.ApproximateGP):
    """Temporal movement primitives. This module contains just the primitives, not the weights.
    Primitives are drawn from a sparse GP prior"""

    def __init__(self, num_MPs, inducing_time_points, init_data):
        """inducing_time_points: time points where the inducing values of the primitives are stored.
        num_MPs: number of movement primitives"""

        variational_distribution = (
            gpytorch.variational.MeanFieldVariationalDistribution(
                inducing_time_points.size(0), batch_shape=torch.Size([num_MPs])
            )
        )
        variational_strategy = wmll.VariationalStrategyWithPosteriorInit(
            self,
            inducing_time_points,
            variational_distribution,
            learn_inducing_locations=False,
        )
        super(TMPModel, self).__init__(variational_strategy)

        self.inducing_time_points = inducing_time_points

        self.mean_module = gpytorch.means.ConstantMean(
            batch_shape=torch.tensor([num_MPs])
        )

        picklable_pos_constraint = gpytorch.constraints.Positive(
            transform=wmll.picklable_softplus, inv_transform=wmll.picklable_inv_softplus
        )
        RBFKern = gpytorch.kernels.RBFKernel(
            batch_shape=torch.tensor([num_MPs]),
            lengthscale_constraint=picklable_pos_constraint,
        )  # lengthscale_constraint=gpytorch.constraints.Interval(0.1,12.0))
        picklable_pos_constraint = gpytorch.constraints.Positive(
            transform=wmll.picklable_softplus, inv_transform=wmll.picklable_inv_softplus
        )
        self.covar_module = gpytorch.kernels.ScaleKernel(
            RBFKern,
            batch_shape=torch.tensor([num_MPs]),
            outputscale_constraint=picklable_pos_constraint,
        )
        # self.covar_module.register_constraint("raw_outputscale", gpytorch.constraints.Interval(0.1,10.0))

    @property
    def device(self):
        return next(self.parameters()).device

    @property
    def time_interval(self):
        return float(self.variational_strategy.inducing_points.max())

    def forward(self, time_points):
        mean_MPs = self.mean_module(time_points)
        self.covar_MPs = self.covar_module(time_points)

        # try:
        #     self.covar_MPs.cholesky()
        # except:
        #     #add jitter (0.001) if diag has negative elements
        #     self.covar_MPs = self.covar_MPs.add_jitter(0.01)
        #     print('adding jitter to the covariance matrix to ensure p.d.')
        #     pass

        return gpytorch.distributions.MultivariateNormal(mean_MPs, self.covar_MPs)

    def init_sampling(self):
        """Initialize the inducing distributions for the MPs by sampling from GP prior"""
        prior_mean = self.forward(self.variational_strategy.inducing_points).sample()
        prior_covar = (
            self.variational_strategy.variational_distribution.covariance_matrix
        )

        self.variational_strategy.initialize_variational_distribution(
            gpytorch.distributions.MultivariateNormal(prior_mean, prior_covar)
        )

    def init_SVD(self, data_segments):
        """Initialize the inducing distributions for the MPs from data_segments. Assumes evenly spaced inducing points.
        data_segments will be resampled to have the same lenghts as the inducing time points, then SVDed"""

        sensors = None
        segments = []
        kernel_width_estimates = []
        num_MPs = self.mean_module(torch.tensor([0.0])).shape[0]

        num_t_points = len(self.variational_strategy.inducing_points)

        for segment in data_segments:

            segment = np.array(segment)

            if sensors is None:
                sensors = len(segment)
            else:
                if sensors != len(segment):
                    raise ValueError(
                        "All segments must have the same number of sensors, but found {0:d} and {1:d}".format(
                            sensors, len(segment)
                        )
                    )

            # resample segment to self.num_t_points
            slen2 = segment.shape[1] // 2
            ctr = np.hstack(
                [
                    np.ones((1, slen2)) * segment[:, 0:1],
                    segment,
                    np.ones((1, slen2)) * segment[:, -2:-1],
                ]
            )  # pad start and end to avoid resampling artifacts
            resampled_segment = scipy.signal.resample(ctr, num_t_points * 2, axis=1)[
                :, num_t_points // 2 : 3 * num_t_points // 2
            ]  # resample and cut out the middle
            segments.append(resampled_segment)

            # estimate RBF kernel width
            sd = np.hstack(resampled_segment)
            cf = (np.correlate(sd, sd, mode="full") / len(sd) - sd.mean() ** 2).clip(
                0.0
            )
            cf = cf[
                len(cf) // 2 - num_t_points // 2 : len(cf) // 2 + num_t_points // 2
            ]  # cut off noisy bits of distribution that would mess up the variance estimate
            cf /= (
                cf.sum()
            )  # normalize autocorrelation to make it interpretable as a probability density
            kernel_width_estimates.append(
                np.sqrt((cf * np.arange(-len(cf) // 2, len(cf) // 2) ** 2).sum())
            )  # compute variance, which is kernel width. Factor 2 for finite-sample-size correction

        concat_segments = np.concatenate(segments, axis=0)

        kernel_width = (
            np.mean(kernel_width_estimates) * self.time_interval / num_t_points
        )
        kernel_scale = concat_segments.var(axis=1).mean()
        self.initialize(
            **{
                "covar_module.outputscale": kernel_scale,
                "covar_module.base_kernel.lengthscale": kernel_width,
            }
        )

        self.MPs = torch.nn.ParameterList()

        prior_covar = (
            self.variational_strategy.variational_distribution.covariance_matrix
        )

        # prior_covar=self.covar_module(torch.arange(0,self.inducing_time_points.size(0),1)).add_jitter(0.001)

        U, S, V = np.linalg.svd(concat_segments)

        self.all_mps = np.dot(np.diag(S[:num_MPs]), V[:num_MPs])
        self.variational_strategy.initialize_variational_distribution(
            gpytorch.distributions.MultivariateNormal(
                torch.tensor(self.all_mps), prior_covar
            )
        )

    def totalKL(self):
        return self.variational_strategy.kl_divergence().sum()


class SimpleWeightModel(gpytorch.Module):
    """Weights for a temporal movement primitive model.
    Weights have a multivariate Gaussian prior across MPs, and are independent across DFs
    This simple weight model is not conditioned on any inputs, i.e. weights have a fixed prior with zero mean and unit variance"""

    def __init__(self, num_DFs, num_MPs):
        """num_DFs: number of degrees of freedom to be modelled, e.g. joint angles
        num_MPs: number of movement primitives to be combined with these weights
        Prior will have mean zero and unit covariance across MPs for each DF
        """
        super(SimpleWeightModel, self).__init__()
        self.register_buffer("weight_prior_mean", torch.zeros((num_DFs, num_MPs)))
        self.register_buffer(
            "weight_prior_covariance_tril", torch.eye(num_MPs).repeat(num_DFs, 1, 1)
        )

        wpm = torch.distributions.MultivariateNormal(
            self.weight_prior_mean, scale_tril=self.weight_prior_covariance_tril
        ).sample()
        self.register_parameter("weight_posterior_mean", torch.nn.Parameter(wpm))

        self.register_parameter(
            "raw_weight_posterior_covariance_tril",
            torch.nn.Parameter(torch.eye(num_MPs).repeat(num_DFs, 1, 1)),
        )
        self.register_constraint(
            "raw_weight_posterior_covariance_tril", wmll.DiagonalGreaterThan(1e-2)
        )

        self.register_parameter(
            "weight_posterior_covariance",
            torch.nn.Parameter(torch.eye(num_MPs).repeat(num_DFs, 1, 1)),
        )

        self.covariance_mode()

    def covariance_mode(self, mode="tril"):
        """Set covariance computation mode.
        if mode='tril': use lower cholesky factor, constrained to positive diagonal
        if mode='full': use full covariance matrix. Switch to this mode if you want to use exact weight_updates"""
        if not hasattr(self, "covariance_mode"):
            self.cov_mode = mode

        if mode == self.covariance_mode:
            return

        if mode == "tril":
            self.weight_posterior_covariance_tril = torch.cholesky(
                self.weight_posterior_covariance.clone().detach()
            )

        if mode == "full":
            self.initialize(
                weight_posterior_covariance=torch.bmm(
                    self.weight_posterior_covariance_tril,
                    self.weight_posterior_covariance_tril.transpose(1, 2),
                ).detach()
            )

        self.cov_mode = mode

    @property
    def weight_posterior_covariance_tril(self):
        return self.raw_weight_posterior_covariance_tril_constraint.transform(
            self.raw_weight_posterior_covariance_tril
        )

    @weight_posterior_covariance_tril.setter
    def weight_posterior_covariance_tril(self, value):
        self.initialize(
            raw_weight_posterior_covariance_tril=self.raw_weight_posterior_covariance_tril_constraint.inverse_transform(
                value
            )
        )

    def forward(self, *inputs):
        if self.cov_mode == "tril":
            return torch.distributions.MultivariateNormal(
                self.weight_posterior_mean,
                scale_tril=self.weight_posterior_covariance_tril,
            )
        elif self.cov_mode == "full":
            return torch.distributions.MultivariateNormal(
                self.weight_posterior_mean, self.weight_posterior_covariance
            )

        raise RuntimeError("Unknown covariance mode", self.cov_mode)

    def detached_posterior(self, *inputs):
        if self.cov_mode == "tril":
            return torch.distributions.MultivariateNormal(
                self.weight_posterior_mean.clone().detach(),
                scale_tril=self.weight_posterior_covariance_tril.clone().detach(),
            )
        elif self.cov_mode == "full":
            return torch.distributions.MultivariateNormal(
                self.weight_posterior_mean.clone().detach(),
                self.weight_posterior_covariance.clone().detach(),
            )

        raise RuntimeError("Unknown covariance mode", self.cov_mode)

    @property  # @cached_property #use only @property if you cannot import from functools import cached_property
    def prior(self):
        return torch.distributions.MultivariateNormal(
            self.weight_prior_mean, scale_tril=self.weight_prior_covariance_tril
        )

    def KL_loss(self, **kwargs):
        """KL-divergence between current posterior and prior"""

        if "weights" in kwargs:
            posterior = kwargs["weights"]
        elif self.cov_mode == "tril":
            posterior = torch.distributions.MultivariateNormal(
                self.weight_posterior_mean,
                scale_tril=self.weight_posterior_covariance_tril,
            )
        else:
            posterior = torch.distributions.MultivariateNormal(
                self.weight_posterior_mean, self.weight_posterior_covariance
            )

        return torch.distributions.kl_divergence(posterior, self.prior).sum()

    def KL_loss_list(self, weight_dist_list, segment_weights):
        """Computes KL-divergence between every entry pf the weight_dist_list and the prior, multplies that with the corresponding entry in segment_weights and sums"""
        return sum(
            [
                sw * torch.distributions.kl_divergence(posterior, self.prior).sum()
                for posterior, sw in zip(weight_dist_list, segment_weights)
            ]
        )

    def weight_update(self):
        """Second-order weight update. Requires gradients on the parameters, computed in 'full' covariance mode"""
        if not self.cov_mode == "full" or self.weight_posterior_covariance.grad is None:
            raise RuntimeError(
                "second-order weight updates require that the covariance derivatives have been computed in 'full' mode"
            )

        with torch.no_grad():

            M = (
                -2.0 * self.weight_posterior_covariance.grad
                + wmll.batch_cholesky_inverse(self.weight_posterior_covariance)
            )
            Minv = wmll.batch_cholesky_inverse(M)

            # sys.stdout.flush()
            self.weight_posterior_covariance.copy_(Minv)
            # self.weight_posterior_mean += torch.bmm(-Minv,-self.weight_posterior_mean.grad.unsqueeze(-1)).squeeze(-1)

            self.weight_posterior_mean = torch.nn.Parameter(
                self.weight_posterior_mean
                + torch.bmm(
                    Minv, self.weight_posterior_mean.grad.unsqueeze(-1)
                ).squeeze(-1)
            )

            mean_weights = self.weight_posterior_mean.clone().detach()  # .copy()
            # self.weight_posterior_mean.grad = None
            # self.weight_posterior_covariance.grad = None
            try:
                return torch.distributions.MultivariateNormal(
                    self.weight_posterior_mean.clone().detach(),
                    Minv,
                    validate_args=False,
                )
            except:
                print("Updated covariance matrix: {}".format(Minv))

                min_1 = []
                min_torch = []

                for m in Minv:
                    # print ('Diagonal of covariance matrix of DF {}: {}'.format(i, np.diagonal(m.numpy())))

                    e_torch = torch.symeig(m)
                    min_torch.append(min(e_torch[0]))

                    e_1 = np.linalg.eigvals(m.numpy())
                    min_1.append(min(e_1))

                print(
                    "minimum overall eigenvalues of np.eigvals are: {} whereas min eigenvalue of torch.symeig is: {}".format(
                        min(min_1), min(min_torch)
                    )
                )

                Minv = Minv.flatten(end_dim=-3)
                boost_val = 1e-3 * Minv.abs().max()

                for b in range(20):  # 6
                    try:
                        return torch.distributions.MultivariateNormal(
                            self.weight_posterior_mean.clone().detach(),
                            Minv,
                            validate_args=False,
                        )
                    except RuntimeError:  # singular matrix, boost diagonals
                        Minv = Minv + torch.eye(Minv.shape[-1]) * boost_val
                        boost_val *= 10.0

                raise RuntimeError(
                    "batch_cholesky_inverse: could not find approximate inverse after boosting"
                )


class TMPModelWithSimpleWeights(torch.nn.Module):
    """TMP model with simple weights"""

    def __init__(
        self,
        num_DFs,
        num_MPs,
        inducing_time_points,
        init_data=None,
        all_data=None,
        MP_model=None,
        weight_model=None,
        noise_target=None,
    ):  # noise_level == noise_target
        super(TMPModelWithSimpleWeights, self).__init__()
        self.MPs = (
            TMPModel(num_MPs, inducing_time_points, init_data)
            if MP_model is None
            else MP_model
        )
        self.weights = (
            SimpleWeightModel(num_DFs, num_MPs)
            if weight_model is None
            else weight_model
        )

        self.init_data = init_data  # segmented data as per heuristic segmentation
        self.all_data = all_data  # continuous data stream
        self.time_points_primitives = inducing_time_points

        if noise_target is None:
            noise_target = 0.1
            noise_lbound = 1e-4
            noise_ubound = 10.0
        else:
            noise_lbound = 0.9 * noise_target
            noise_ubound = 1.1 * noise_target

        self.likelihood = gpytorch.likelihoods.MultitaskGaussianLikelihood(
            num_tasks=num_DFs,
            rank=0,
            noise_prior=gpytorch.priors.SmoothedBoxPrior(noise_lbound, noise_ubound),
            noise_constraint=gpytorch.constraints.Interval(noise_lbound, noise_ubound),
            has_task_noise=False,
        )

        self.likelihood.noise = noise_target

        self._MP_loss = wmll.WeightedMarginalLogLikelihood(self.likelihood, self.MPs)

        self.weights_dict = dict()

        self.training_mode()

    @property
    def num_data(self):
        return self._MP_loss.num_data

    @num_data.setter
    def num_data(self, val):
        self._MP_loss.num_data = val

    def forward(self, time_points, **kwargs):

        MP_dist = self.MPs(time_points)
        if "weights" in kwargs:
            W_dist = kwargs["weights"]
        else:
            W_dist = self.weights()

        mean_df = torch.mm(W_dist.mean, MP_dist.mean)

        var_df = (
            torch.mm(W_dist.mean ** 2, MP_dist.variance)
            + torch.mm(W_dist.variance, MP_dist.variance)
            + torch.einsum(
                "isq,st,qt->it", W_dist.covariance_matrix, MP_dist.mean, MP_dist.mean
            )
        )

        all_covars = [gpytorch.lazy.DiagLazyTensor(vdf).unsqueeze(0) for vdf in var_df]
        covar_df = gpytorch.lazy.CatLazyTensor(*all_covars)

        rdist = gpytorch.distributions.MultitaskMultivariateNormal.from_batch_mvn(
            gpytorch.distributions.MultivariateNormal(mean_df, covar_df), task_dim=0
        )
        return rdist

    def sample(self, time_points):

        MPs = self.MPs(time_points).mean
        W_samples = self.weights().sample()

        return torch.mm(W_samples, MPs)

    def inferWeights_per_segment_binner(self, bin_start, bin_end):
        """Infer weights for data in training_data_list, using the entries of time_point_list as inputs
        returns list of weight distributions, and inference time"""

        self.weights.covariance_mode("full")
        self.training_mode(weights=True)

        time_points = torch.arange(
            0.0,
            len(self.time_points_primitives.tolist()),
            len(self.time_points_primitives.tolist()) / (bin_end - bin_start),
        )

        if time_points[-1] == len(self.time_points_primitives.tolist()):
            time_points = time_points[:-1]

        self.zero_grad()
        loss = self.weight_loss(bin_start, bin_end)
        loss.backward()
        updated_weights = self.weights.weight_update()

        self.weights_dict[(bin_start, bin_end)] = updated_weights

        self.training_mode(weights=False)

        return updated_weights

    def inferWeights(self, time_point_list, data_list):
        """Infer weights for data in training_data_list, using the entries of time_point_list as inputs
        returns list of weight distributions, and inference time"""

        # compute weights for each segment
        self.training_mode(weights=True)
        self.weights.covariance_mode("full")
        weight_dists = []
        for tp, data in zip(
            time_point_list, data_list
        ):  # in target application, this loop is done by the binner

            self.zero_grad()
            loss = self.weight_loss(tp, data)
            loss.backward()
            weight_dists.append(self.weights.weight_update())

        self.training_mode(weights=False)

        return weight_dists

    def weight_loss(self, time_points, data, **kwargs):

        predictions = self(time_points, **kwargs)
        return -self.likelihood.expected_log_prob(
            data.T, predictions
        ).sum() + self.weights.KL_loss(**kwargs)

    def weight_loss_binner(self, start, end, **kwargs):

        time_points = torch.arange(
            0.0,
            len(self.time_points_primitives.tolist()),
            len(self.time_points_primitives.tolist()) / (end - start),
        )

        if time_points[-1] == len(self.time_points_primitives.tolist()):
            time_points = time_points[:-1]

        predictions = self(time_points, **kwargs)
        return self.likelihood.expected_log_prob(
            torch.from_numpy(self.all_data[start:end]), predictions
        ).sum() - self.weights.KL_loss(**kwargs)

    def MP_loss(self, time_point_list, data_list, weight_list, segment_weight_list):

        predictions = [
            self(tp, weights=w) for tp, w in zip(time_point_list, weight_list)
        ]
        return -self._MP_loss(predictions, data_list, logl_weights=segment_weight_list)

    def segment_weighted_ELBO(
        self, time_point_list, data_list, weight_list, segment_weight_list
    ):
        return -self.MP_loss(
            time_point_list, data_list, weight_list, segment_weight_list
        ) - self.weights.KL_loss_list(weight_list, segment_weight_list)

    def training_mode(self, weights=False, mps=False, likelihood=False):
        self.weights.requires_grad_(weights)
        self.MPs.requires_grad_(mps)
        self.likelihood.requires_grad_(likelihood)

    def unroll_data(self, time_point_list, data_list, weight_list, segment_weight_list):
        """Reshape data and weights for ELBO evaluation without needing to iterate through lists"""
        all_time_points = torch.cat(time_point_list)
        all_data = torch.cat(data_list, dim=-1).permute(1, 0)
        all_segment_weights = torch.cat(
            [
                torch.ones(len(tp)) * sw
                for tp, sw in zip(time_point_list, segment_weight_list)
            ],
            dim=-1,
        )
        all_weight_means = torch.cat(
            [
                w.mean.unsqueeze(-1).expand(-1, -1, len(tp))
                for tp, w in zip(time_point_list, weight_list)
            ],
            dim=-1,
        ).permute(2, 0, 1)
        all_weight_covars = torch.cat(
            [
                w.covariance_matrix.unsqueeze(-1).expand(-1, -1, -1, len(tp))
                for tp, w in zip(time_point_list, weight_list)
            ],
            dim=-1,
        ).permute(3, 0, 1, 2)
        return torch.utils.data.TensorDataset(
            all_time_points,
            all_data,
            all_segment_weights,
            all_weight_means,
            all_weight_covars,
        )

    def unrolled_forward(self, unrolled_inputs):
        """predictive distribution for inputs returned by unroll_data()"""

        (
            all_time_points,
            all_data,
            all_segment_weights,
            all_weight_means,
            all_weight_covars,
        ) = unrolled_inputs

        MP_dist = self.MPs(all_time_points)

        mean_df = torch.einsum("tis,st->it", all_weight_means, MP_dist.mean)
        var_df = torch.einsum("tis,st->it", all_weight_means ** 2, MP_dist.variance)
        var_df += torch.einsum("tiss,st->it", all_weight_covars, MP_dist.variance)
        var_df += torch.einsum(
            "tisr,st,rt->it", all_weight_covars, MP_dist.mean, MP_dist.mean
        )

        # print ('Mean MPs: ' + str(MP_dist.mean))

        all_covars = [gpytorch.lazy.DiagLazyTensor(vdf).unsqueeze(0) for vdf in var_df]
        covar_df = gpytorch.lazy.CatLazyTensor(*all_covars)

        rdist = gpytorch.distributions.MultitaskMultivariateNormal.from_batch_mvn(
            gpytorch.distributions.MultivariateNormal(mean_df, covar_df), task_dim=0
        )
        return rdist

    def unrolled_MP_loss(self, unrolled_inputs):
        predictions = self.unrolled_forward(unrolled_inputs)
        (
            all_time_points,
            all_data,
            all_segment_weights,
            all_weight_means,
            all_weight_covars,
        ) = unrolled_inputs

        return -self._MP_loss(predictions, all_data.T, logl_weights=all_segment_weights)

    def unrolled_data_to_same_device(self, unrolled_inputs):
        return tuple([dd.to(self.MPs.device) for dd in unrolled_inputs])


##############################################################
############# Unittests from here ############################
##############################################################


class testTMPModel(unittest.TestCase):
    def setUp(self):

        # model with 5 joint angles, 3 primitives and inducing points at [0,1,..,19] for weight update testing
        self.num_mps = 3
        tmpm = TMPModelWithSimpleWeights(
            5, self.num_mps, torch.arange(20.0), init_data=[], all_data=[]
        )
        tmpm.MPs.initialize(
            **{
                "covar_module.outputscale": 1.0,
                "covar_module.base_kernel.lengthscale": 5.0,
            }
        )
        tmpm.MPs.init_sampling()

        # data for weight update testing
        self.time_points = torch.arange(0.0, 20.0, 0.1)
        self.weight_training_data = tmpm.sample(self.time_points)

        # data for MP update testing
        self.time_point_list = []
        self.MP_training_data_list = []
        self.num_data = 0
        for segments in range(300):
            if segments == 0:
                seg_points = 50
            else:
                seg_points = np.random.randint(5, 51)
            tp = torch.arange(0.0, 20.0, 20.0 / seg_points)
            data = tmpm.sample(tp)
            self.num_data += len(tp)
            self.time_point_list.append(tp)
            self.MP_training_data_list.append(data)
        self.segment_weights = torch.ones(len(self.time_point_list))

    def VAF(self, time_point_list, MP_training_data_list, weight_dists, model):
        old_mode = model.training
        model.eval()
        residual = sum(
            [
                ((dat.T - model(tp, weights=w).mean) ** 2).sum()
                for tp, dat, w in zip(
                    time_point_list, MP_training_data_list, weight_dists
                )
            ]
        )
        all_data = torch.cat(MP_training_data_list, dim=1)
        model.train(old_mode)

        return float(
            1.0 - residual / all_data.numel() / all_data.var(dim=1).mean().detach()
        )

    def inferWeights(self, time_point_list, training_data_list, model):
        """Infer weights for data in training_data_list, using the entries of time_point_list as inputs
        returns list of weight distributions, and inference time"""

        # compute weights for each segment
        start_time = time.time()
        model.training_mode(weights=True)
        model.weights.covariance_mode("full")
        weight_dists = []
        for tp, data in zip(
            time_point_list, training_data_list
        ):  # in target application, this loop is done by the binner

            model.zero_grad()
            loss = model.weight_loss(tp, data)
            loss.backward()
            model.weights.weight_update()
            weight_dists.append(model.weights.detached_posterior())
            loss_after = model.weight_loss(tp, data).detach()
            self.assertTrue(
                loss_after <= loss * 1.0001,
                msg="before {0:f}, after {1:f}".format(float(loss), float(loss_after)),
            )

        weight_time = time.time() - start_time
        return weight_dists, weight_time

    def test4ModelComparison(self):
        """Test if correct model complexity is recovered by ELBO-comparison"""

        SWELBOs = []
        VAF = 0.0
        for num_mps in range(1, 2 * self.num_mps):

            print("Computing model with ", num_mps, "MPs")

            compute_OK = False

            while not compute_OK:
                try:
                    tmpm_tensor = TMPModelWithSimpleWeights(
                        5, num_mps, torch.arange(20.0), init_data=[], dall_data=[]
                    )
                    tmpm_tensor.num_data = self.num_data
                    tmpm_tensor.train()

                    for em_iter in range(20):
                        print("   starting em-iteration", em_iter)

                        weight_dists, weight_time = self.inferWeights(
                            self.time_point_list,
                            self.MP_training_data_list,
                            tmpm_tensor,
                        )

                        # preprare flattended data
                        if use_cuda:  # on a GTX 960, MP updates are 6x faster on GPU
                            tmpm_tensor = tmpm_tensor.cuda()

                        tmpm_tensor.training_mode(
                            mps=True, likelihood=(VAF > 0.95) or (em_iter > 10)
                        )
                        unrolled_data = tmpm_tensor.unroll_data(
                            self.time_point_list,
                            self.MP_training_data_list,
                            weight_dists,
                            self.segment_weights,
                        )
                        data_loader = torch.utils.data.DataLoader(
                            unrolled_data, batch_size=1024, pin_memory=True
                        )

                        optimizer = torch.optim.LBFGS(
                            itertools.chain(tmpm_tensor.parameters()), lr=0.1
                        )

                        last_loss = 1e100

                        for epoch in range(10):

                            def closure():

                                optimizer.zero_grad()
                                total_loss = torch.tensor(0.0)
                                for unrolled_inputs in data_loader:
                                    loss = tmpm_tensor.unrolled_MP_loss(
                                        tmpm_tensor.unrolled_data_to_same_device(
                                            unrolled_inputs
                                        )
                                    )
                                    loss.backward()
                                    total_loss += loss.detach()

                                return total_loss

                            optimizer.step(closure)

                            # termination check
                            new_loss = closure().cpu().detach().numpy()
                            print(
                                "     at iteration",
                                epoch,
                                "loss is",
                                new_loss,
                                "noise level",
                                tmpm_tensor.likelihood.noise,
                            )
                            if (last_loss - new_loss) < 1e-3:
                                break
                            last_loss = new_loss

                        # move model back to cpu for weight updates, which are 6x slower on the GPU
                        tmpm_tensor = tmpm_tensor.cpu()
                        VAF = self.VAF(
                            self.time_point_list,
                            self.MP_training_data_list,
                            weight_dists,
                            tmpm_tensor,
                        )
                        print("   after em-iteration", em_iter, "VAF=", VAF)
                        if VAF > 0.99:
                            break
                    compute_OK = True
                except:
                    pass  # get here if some cholesky decomp fails. restart model in that case

            SWELBOs.append(
                float(
                    tmpm_tensor.segment_weighted_ELBO(
                        self.time_point_list,
                        self.MP_training_data_list,
                        weight_dists,
                        self.segment_weights,
                    ).detach()
                )
            )
            print("Model with ", num_mps, "MPs has weighted ELBO", SWELBOs[-1])

        plt.clf()

        SWELBOs = np.array(SWELBOs)
        SWELBOs -= np.logaddexp.reduce(SWELBOs)

        plt.bar(np.arange(len(SWELBOs)) + 1.0, SWELBOs)
        plt.annotate(
            "*",
            (self.num_mps, SWELBOs.max()),
            fontsize=20,
            horizontalalignment="center",
        )
        plt.savefig("modecmp_test.png", dpi=150)
        plt.show()

    def test1MPLearning_tensor(self):
        """Test MP learning after unrolling all data and weights into a flat tensor format for GPU speedup"""

        print("Testing tensor-based MP updates. Using GPU if available")

        tmpm_tensor = TMPModelWithSimpleWeights(
            5, 3, torch.arange(20.0), init_data=[], all_data=[]
        )
        tmpm_tensor.num_data = self.num_data
        tmpm_tensor.MPs.init_SVD(self.MP_training_data_list)

        # initial weights
        weight_dists = [tmpm_tensor.weights.prior] * len(self.time_point_list)

        SWELBO_old = -1e100

        tmpm_tensor.train()
        for em_iter in range(50):

            SWELBO_new = float(
                tmpm_tensor.segment_weighted_ELBO(
                    self.time_point_list,
                    self.MP_training_data_list,
                    weight_dists,
                    self.segment_weights,
                ).detach()
            )

            print(
                "Iteration",
                em_iter,
                "ELBO before weight update",
                SWELBO_new,
                "VAF=",
                self.VAF(
                    self.time_point_list,
                    self.MP_training_data_list,
                    weight_dists,
                    tmpm_tensor,
                ),
            )
            self.assertTrue(SWELBO_new > SWELBO_old)
            SWELBO_old = SWELBO_new

            weight_dists, weight_time = self.inferWeights(
                self.time_point_list, self.MP_training_data_list, tmpm_tensor
            )

            print("weight update time", weight_time)

            # plot first segment
            plt.clf()
            colors = ["red", "green", "blue", "orange", "black"]
            for dat, col in zip(self.MP_training_data_list[0], colors):
                plt.plot(
                    self.time_point_list[0].cpu(),
                    dat.cpu(),
                    color=col,
                    linewidth=2,
                    linestyle="dashed",
                )
            pred = tmpm_tensor(self.time_point_list[0], weights=weight_dists[0])
            for dat, col in zip(pred.mean.T, colors):
                plt.plot(
                    self.time_point_list[0].cpu(),
                    dat.cpu(),
                    color=col,
                    linewidth=1,
                    linestyle="solid",
                )
            plt.show()

            # optimize MPs using the unrolled representation

            SWELBO_new = float(
                tmpm_tensor.segment_weighted_ELBO(
                    self.time_point_list,
                    self.MP_training_data_list,
                    weight_dists,
                    self.segment_weights,
                ).detach()
            )
            VAF = self.VAF(
                self.time_point_list,
                self.MP_training_data_list,
                weight_dists,
                tmpm_tensor,
            )

            tmpm_tensor.training_mode(
                mps=True, likelihood=(em_iter > 10) or (VAF > 0.95)
            )

            print("ELBO after weight update, before MP update", SWELBO_new, "VAF=", VAF)
            self.assertTrue(SWELBO_new > SWELBO_old)
            SWELBO_old = SWELBO_new

            # preprare flattended data
            if use_cuda:  # on a GTX 960, MP updates are 6x faster on GPU
                tmpm_tensor = tmpm_tensor.cuda()

            unrolled_data = tmpm_tensor.unroll_data(
                self.time_point_list,
                self.MP_training_data_list,
                weight_dists,
                self.segment_weights,
            )

            if use_cuda:
                mem_avail = torch.cuda.get_device_properties(
                    torch.cuda.current_device()
                ).total_memory
                datapoint_size = sum(
                    [d.numel() * d.element_size() for d in unrolled_data[0]]
                )
                max_batch_size = (
                    mem_avail / datapoint_size
                )  # way too much. How can we compute the maximal batch size for a given amout of cuda mem?

            data_loader = torch.utils.data.DataLoader(
                unrolled_data, batch_size=4096, pin_memory=True
            )

            optimizer = torch.optim.LBFGS(
                itertools.chain(tmpm_tensor.parameters()), lr=0.1
            )

            start_time = time.time()

            last_loss = 1e100

            for epoch in range(10):

                def closure():

                    optimizer.zero_grad()
                    total_loss = torch.tensor(0.0)
                    for unrolled_inputs in data_loader:
                        loss = tmpm_tensor.unrolled_MP_loss(
                            tmpm_tensor.unrolled_data_to_same_device(unrolled_inputs)
                        )
                        loss.backward()
                        total_loss = total_loss + loss.detach()

                    return total_loss

                optimizer.step(closure)

                # termination check
                new_loss = closure().cpu().detach().numpy()
                print(
                    "at iteration",
                    epoch,
                    "loss is",
                    new_loss,
                    "noise level",
                    tmpm_tensor.likelihood.noise,
                )
                if (last_loss - new_loss) < 0.1:
                    break
                last_loss = new_loss

            mp_time = time.time() - start_time
            print("MP update time, tensor based learning", mp_time)

            # move model back to cpu for weight updates, which are 6x slower on the GPU
            tmpm_tensor = tmpm_tensor.cpu()
            if VAF > 0.99:
                break

    def test2MPLearning_list(self):
        """Test list-based MP learning"""

        print("Testing list-based MP updates")

        tmpm_list = TMPModelWithSimpleWeights(
            5, 3, torch.arange(20.0), init_data=[], all_data=[]
        )
        tmpm_list.num_data = self.num_data

        weight_dists = [tmpm_list.weights.prior] * len(self.time_point_list)

        SWELBO_old = -1e100

        tmpm_list.train()
        for em_iter in range(50):

            SWELBO_new = float(
                tmpm_list.segment_weighted_ELBO(
                    self.time_point_list,
                    self.MP_training_data_list,
                    weight_dists,
                    self.segment_weights,
                ).detach()
            )
            self.assertTrue(SWELBO_new > SWELBO_old)
            SWELBO_old = SWELBO_new
            print(
                "ELBO before weight update",
                SWELBO_new,
                "VAF=",
                self.VAF(
                    self.time_point_list,
                    self.MP_training_data_list,
                    weight_dists,
                    tmpm_list,
                ),
            )

            # compute weights for each segment
            start_time = time.time()
            tmpm_list.training_mode(weights=True)
            tmpm_list.weights.covariance_mode("full")
            weight_dists = []
            for tp, data in zip(self.time_point_list, self.MP_training_data_list):

                tmpm_list.zero_grad()
                loss = tmpm_list.weight_loss(tp, data)
                loss.backward()
                tmpm_list.weights.weight_update()
                weight_dists.append(tmpm_list.weights.detached_posterior())
                loss_after = tmpm_list.weight_loss(tp, data).detach()
                self.assertTrue(
                    loss_after <= loss * 1.0001,
                    msg="before {0:f}, after {1:f}".format(
                        float(loss), float(loss_after)
                    ),
                )

            weight_time = time.time() - start_time
            print("weight update time", weight_time)

            plt.clf()
            colors = ["red", "green", "blue", "orange", "black"]
            for dat, col in zip(self.MP_training_data_list[0], colors):
                plt.plot(
                    self.time_point_list[0].cpu(),
                    dat.cpu(),
                    color=col,
                    linewidth=2,
                    linestyle="dashed",
                )
            pred = tmpm_list(self.time_point_list[0], weights=weight_dists[0])
            for dat, col in zip(pred.mean.T, colors):
                plt.plot(
                    self.time_point_list[0].cpu(),
                    dat.cpu(),
                    color=col,
                    linewidth=1,
                    linestyle="solid",
                )
            plt.show()

            # optimize MPs
            tmpm_list.training_mode(mps=True)
            SWELBO_new = float(
                tmpm_list.segment_weighted_ELBO(
                    self.time_point_list,
                    self.MP_training_data_list,
                    weight_dists,
                    self.segment_weights,
                ).detach()
            )
            self.assertTrue(SWELBO_new > SWELBO_old)
            SWELBO_old = SWELBO_new
            VAF = self.VAF(
                self.time_point_list,
                self.MP_training_data_list,
                weight_dists,
                tmpm_list,
            )
            print("ELBO after weight update, before MP update", SWELBO_new, "VAF=", VAF)

            optimizer = torch.optim.LBFGS(
                itertools.chain(tmpm_list.parameters()), lr=0.1
            )

            start_time = time.time()

            last_loss = 1e100
            for epoch in range(10):

                def closure():

                    optimizer.zero_grad()
                    loss = tmpm_list.MP_loss(
                        self.time_point_list,
                        self.MP_training_data_list,
                        weight_dists,
                        self.segment_weights,
                    )
                    loss.backward()

                    return loss

                optimizer.step(closure)

                # termination check
                new_loss = closure().cpu().detach().numpy()
                print("at iteration", epoch, "loss is", new_loss)
                if (last_loss - new_loss) < 1e-4:
                    break
                last_loss = new_loss

            mp_time = time.time() - start_time
            print("MP update time, list based learning", mp_time)
            if VAF > 0.99:
                break

    def test3WeightUpdates(self):
        """Test if optimizer-driven weight updates yield the same result as direct second-order weight updates for a Gaussian likelihood"""

        print("Testing direct weight updates against optimizer-driven solution")

        # TMP model for exact weight updates
        tmpm_exact = TMPModelWithSimpleWeights(
            5, 3, torch.arange(20.0), init_data=[], all_data=[]
        )
        tmpm_exact.MPs.init_sampling()
        tmpm_exact.num_data = len(self.time_points)

        # prior VAF (Variance Accounted For)
        tmpm_exact.eval()
        VAF_prior = (
            1.0
            - (
                (self.weight_training_data - tmpm_exact(self.time_points).mean.T) ** 2
            ).mean()
            / self.weight_training_data.var()
        )

        start_time = time.time()

        tmpm_exact.train()
        tmpm_exact.training_mode(weights=True)
        # exact updates work only in full covariance mode
        tmpm_exact.weights.covariance_mode("full")

        loss = tmpm_exact.weight_loss(self.time_points, self.weight_training_data)
        # gradients need to be computed before weight update
        loss.backward()
        tmpm_exact.weights.weight_update()
        best_exact_loss = float(
            tmpm_exact.weight_loss(self.time_points, self.weight_training_data).detach()
        )
        tmpm_exact.zero_grad()

        print("Exact update took", time.time() - start_time, "seconds")

        # check if gradients are zero at the optimum
        loss = tmpm_exact.weight_loss(self.time_points, self.weight_training_data)
        loss.backward()
        self.assertTrue(
            torch.norm(tmpm_exact.weights.weight_posterior_covariance.grad) <= 1e-3
        )
        self.assertTrue(
            torch.norm(tmpm_exact.weights.weight_posterior_mean.grad) <= 1e-3
        )

        # posterior VAF
        tmpm_exact.eval()
        VAF_posterior = (
            1.0
            - (
                (self.weight_training_data - tmpm_exact(self.time_points).mean.T) ** 2
            ).mean()
            / self.weight_training_data.var()
        )
        self.assertTrue(VAF_posterior > VAF_prior)

        # TMP model for optimizer-driven weight update. Reuses MP model for comparability of computed weights
        tmpm_opt = TMPModelWithSimpleWeights(5, 3, torch.arange(20.0), tmpm_exact.MPs)
        tmpm_opt.num_data = len(self.time_points)
        tmpm_opt.training_mode(weights=True)
        # lower triangular Cholesky factor representation of covariance, for faster constrained optimization
        tmpm_opt.weights.covariance_mode("tril")

        optimizer = torch.optim.LBFGS(itertools.chain(tmpm_opt.parameters()), lr=0.01)

        start_time = time.time()

        last_loss = 1e100
        for epoch in range(100):

            def closure():
                optimizer.zero_grad()
                loss = tmpm_opt.weight_loss(self.time_points, self.weight_training_data)
                loss.backward()
                return loss

            optimizer.step(closure)

            # termination check
            new_loss = closure().cpu().detach().numpy()
            if epoch % 10 == 0:
                print(
                    "at iteration",
                    epoch,
                    "loss is",
                    new_loss,
                    "exact loss",
                    best_exact_loss,
                )
            if (last_loss - new_loss) < 1e-4:
                break
            last_loss = new_loss

        best_opt_loss = float(new_loss)

        print("Training took", time.time() - start_time, "seconds")

        # exact solution must be as good as optimizer solution within rounding tolerances
        self.assertAlmostEqual(
            best_exact_loss, best_opt_loss, delta=best_exact_loss * 0.0001
        )
        # weights should be very similar, too
        self.assertTrue(
            torch.allclose(
                tmpm_exact.weights.weight_posterior_mean,
                tmpm_opt.weights.weight_posterior_mean,
                rtol=1e-3,
                atol=1e-3,
            )
        )


if __name__ == "__main__":

    unittest.main(verbosity=2)
