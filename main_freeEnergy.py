# -*- coding: utf-8 -*-
"""
Created on Wed Nov  6 11:06:54 2019

@authors: Anja
"""
import sys
import os

sys.path.append(os.path.realpath(__file__))
workdir = os.getcwd()

sys.path.append(workdir)

import freeEnergyTMP as mp
import readData

# import provider
import torch
import math
import numpy as np
import itertools

# import threadpoolctl

import matplotlib

# matplotlib.use("Agg")
matplotlib.use("Qt4Agg")
import matplotlib.pyplot as plt

from matplotlib import cm
import os
import pickle
import time
import unittest
import scipy
import warnings
import gc
from collections import defaultdict


def get_VAF(time_point_list, seg_data, weight_dist, model):
    old_mode = model.training
    model.eval()
    residual = sum(
        [
            ((dat.T - model(tp, weights=w).mean) ** 2).sum()
            for tp, dat, w in zip(time_point_list, seg_data, weight_dist)
        ]
    )
    all_data = torch.cat(seg_data, dim=1)

    return float(
        1.0 - residual / all_data.numel() / all_data.var(dim=1).mean().detach()
    )


def plot(time_point_list, seg_data, weight_dist, model):
    model.eval()
    for tp, dat, w in zip(time_point_list, seg_data, weight_dist):
        ls = plt.plot(tp, dat.T, 'x')
        plt.gca().set_prop_cycle(None)
        plt.plot(tp, model(tp, weights=w).mean, '--')
        plt.show()


def getParent(path, levels=1):
    common = path

    # Using for loop for getting
    # starting point
    for i in range(levels):

        # Starting point
        common = os.path.dirname(common)

    return common


def makedir(dirname):
    if not os.path.isdir(dirname):
        os.makedirs(dirname)


def learn_model(
    seg_data,
    num_timepoints_per_primitive,
    num_MPs,
    max_runs,
    dir_picklefile,
    adam_steps=10,
    lbfgs_steps=10,
):

    use_cuda = torch.cuda.is_available()
    # use_cuda = False
    
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

    print("Use cuda: " + str(use_cuda))

    path_results = workdir + "/Results/" + dir_picklefile

    makedir(path_results)

    prob_data_given_num_mps = dict()

    for num_mps in num_MPs:
        num_DFs = 6

        print("NumPrim: " + str(num_mps))
        print("NumDFs: " + str(num_DFs))

        # seg_data[JxT]
        # print('N_segments: ' + str(len(seg_data)))

        #### Init MP Model
        # noise level: 0.01 times signal variance, empirically tested by Clever et al., 2016
        noise_level = np.mean([float(t.var(axis=1).mean()) for t in seg_data]) * 0.01 * 100
        print('noise level:', noise_level)

        seg_data = [torch.DoubleTensor(d) for d in seg_data]
        MP_model = mp.TMPModelWithSimpleWeights(
            num_DFs=num_DFs,
            num_MPs=num_mps,
            inducing_time_points=torch.arange(num_timepoints_per_primitive),
            # init_data=seg_data,
            # all_data=[],
            noise_target=noise_level,
        )  # 0.1 ) #  #optimize_weights = True
        MP_model.MPs.init_SVD(seg_data)

        mps_start = MP_model.MPs.all_mps

        weights_start = [
            w.clone().detach() for w in MP_model.weights.weight_posterior_mean
        ]

        # enable tracking by torch autograd if error arise
        # torch.autograd.set_detect_anomaly(True)

        ELBO_new = []
        ELBO_old = 0
        run = 0
        VAF_list = []

        inducing_time_points = [
            torch.arange(
                0.0,
                num_timepoints_per_primitive
                - (num_timepoints_per_primitive / len(s.T) / 2),
                num_timepoints_per_primitive / (len(s.T)),
                dtype=torch.double,
            )
            for s in seg_data
        ]

        ###start n many optimization runs
        while run < max_runs:
            MP_model = MP_model.cuda().cpu()

            print("Run: " + str(run))
            # MP_model.i_config = 0

            segment_weights_init = torch.ones(
                len(seg_data), dtype=torch.double
            )  # at initialisation all segments have same probability

            ###optimize weights
            opt_weights_dist = MP_model.inferWeights(inducing_time_points, seg_data)
            # print('infer weights:', opt_weights_dist)

            ###compute ELBO after Weight Update, before MP Update
            ELBO_old = float(
                MP_model.segment_weighted_ELBO(
                    inducing_time_points,
                    seg_data,
                    opt_weights_dist,
                    segment_weights_init,
                ).detach()
            )

            print("SWELBO after weight update, before MP update: " + str(ELBO_old))

            # optimize MPs
            if use_cuda:  # on a GTX 960, MP updates are 6x faster on GPU
                MP_model = MP_model.cuda()

            # unroll data to ship on GPU
            unrolled_data_init = MP_model.unroll_data(
                inducing_time_points, seg_data, opt_weights_dist, segment_weights_init
            )

            data_loader = torch.utils.data.DataLoader(
                unrolled_data_init, batch_size=1000, num_workers=0, pin_memory=True
            )

            last_loss = 1e100
            optimizer1 = torch.optim.Adam(MP_model.parameters(), lr=0.01)
            optimizer2 = torch.optim.LBFGS(
                itertools.chain(MP_model.parameters()), lr=0.1, max_iter=20
            )  # parameters := MPs

            MP_model.training_mode(mps=True)

            def closure():

                optimizer.zero_grad()

                if use_cuda:
                    total_loss = torch.tensor(0.0).cuda()
                else:
                    total_loss = torch.tensor(0.0)

                for unrolled_inputs in data_loader:
                    # print('device: ', MP_model.MPs.device, unrolled_inputs[0].device)
                    ur_data = MP_model.unrolled_data_to_same_device(unrolled_inputs)
                    # print('urdata', ur_data[0].device)
                    loss = MP_model.unrolled_MP_loss(ur_data)
                    loss.backward()
                    total_loss += loss.detach()

                return total_loss

            for epoch in range(adam_steps + lbfgs_steps):

                if epoch < adam_steps:
                    optimizer = optimizer1
                else:
                    optimizer = optimizer2

                # with threadpoolctl.threadpool_limits(limits=num_thread):
                # print("# with threadpoolctl.threadpool_limits(limits=num_thread):")
                optimizer.step(closure)
                # print("optimizer.step(closure)")

                # termination check
                new_loss = closure().cpu().detach().numpy()
                print(
                    "     at iteration",
                    epoch,
                    "loss is",
                    new_loss,
                    "noise level",
                    MP_model.likelihood.noise,
                )

                if (last_loss - new_loss) < 1e-3:
                    break
                last_loss = new_loss

            MP_model.training_mode(mps=False)

            # move model back to cpu
            if use_cuda:  # on a GTX 960, MP updates are 6x faster on GPU
                MP_model = MP_model.cpu()

            ###compute ELBO and VAF after MP Update
            ELBO_new.append(
                float(
                    # MP_model.segment_weighted_ELBO(
                    MP_model.segment_weighted_ELBO(
                        inducing_time_points,
                        seg_data,
                        opt_weights_dist,
                        segment_weights_init,
                    ).detach()
                )
            )

            # VAF
            VAF = get_VAF(inducing_time_points, seg_data, opt_weights_dist, MP_model)
            plot(inducing_time_points, seg_data, opt_weights_dist, MP_model)
            VAF_list.append(VAF)

            print(
                "ELBO after weight update and after MP update: "
                + str(ELBO_new[-1])
                + "and VAF: "
                + str(VAF_list[-1])
            )

            if (np.abs(ELBO_old - ELBO_new[-1])) < 1e-2:
                break
            else:
                run += 1

        # save MPs
        time_points = torch.arange(0.0, num_timepoints_per_primitive, 1)
        optim_mps = MP_model.MPs(time_points).mean

        print('noise level:', noise_level)

        # Save results
        prob_data_given_num_mps[num_mps] = (
            opt_weights_dist,
            optim_mps,
            ELBO_new,
            VAF_list,
            mps_start,
            weights_start,
        )
        torch.save(prob_data_given_num_mps, os.path.join(path_results, "results.pkl"))
        torch.save(
            MP_model.state_dict(),
            os.path.join(path_results, "Model_MP" + str(num_mps) + ".pkl"),
        )

        gc.collect()
        del MP_model


class testMP(unittest.TestCase):
    def setUp(self):

        self.num_timepoints_per_primitive = 50
        self.num_ground_truth_segments = 3
        self.num_ground_truth_signals = 10
        self.num_ground_truth_MPs = 4
        self.ground_truth_kernel_width = 5.0

        MP_model_init = mp.TMPModelWithSimpleWeights(
            num_DFs=self.num_ground_truth_signals,
            num_MPs=self.num_ground_truth_MPs,
            inducing_time_points=torch.arange(self.num_timepoints_per_primitive),
            init_data=[],
            all_data=[],
        )
        MP_model_init.MPs.initialize(
            **{
                "covar_module.outputscale": 1.0,
                "covar_module.base_kernel.lengthscale": 1.0,
            }
        )
        MP_model_init.MPs.init_sampling()

        # data for weight update testing
        self.time_points = torch.arange(0.0, self.num_timepoints_per_primitive, 0.1)
        self.weight_training_data = MP_model_init.sample(self.time_points)

        # data for MP update testing
        self.time_point_list = []
        seg_points_list = []
        self.MP_training_data_list = []
        self.num_data = 0
        for segments in range(300):
            if segments == 0:
                seg_points = 50
            else:
                seg_points = np.random.randint(
                    5, 2 * self.num_timepoints_per_primitive
                )  # ,size=self.num_ground_truth_segments
                seg_points_list.append(seg_points)
            tp = torch.arange(
                0.0,
                self.num_timepoints_per_primitive,
                self.num_timepoints_per_primitive / seg_points,
            )
            data = MP_model_init.sample(tp)
            self.num_data += len(tp)
            self.time_point_list.append(tp)
            self.MP_training_data_list.append(data)
        self.segment_weights = torch.ones(len(self.time_point_list))

        # ground_truth_data
        all_data = np.hstack(self.MP_training_data_list)

        self.all_data = all_data.T
        self.tmax = len(self.all_data)
        self.ground_truth_bounds = [0]
        for i in range(len(seg_points_list)):
            self.ground_truth_bounds.append(
                self.ground_truth_bounds[-1] + seg_points_list[i]
            )

        print("done")

    def segmentData(data, bounds):
        data = data.T
        segments = []
        for i in range(len(bounds) - 1):
            segments.append(data[:, bounds[i] : bounds[i + 1]])
        dataSegmented = segments
        return dataSegmented


if __name__ == "__main__":

    execute_unittest = False

    # to ensure that all tensors have same type (torch.float64)
    torch.set_default_tensor_type(torch.DoubleTensor)

    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    vp, block, condition = 1234, 2, "2"
    data, meta = readData.read_data(vp, block)
    # data = [d for d, m in zip(data, meta.condition) if m == condition]
    print(len(data))

    # all_data = data['all_data']
    # segmented_data= data['seg_data']

    # print("Data shape:" + str(all_data.shape))

    if execute_unittest:
        unittest.main()
    else:
        learn_model(
            seg_data=data,
            num_timepoints_per_primitive=30,
            # num_MPs=np.arange(3, 8, 1),
            num_MPs=np.arange(20, 21, 1),
            max_runs=1,
            dir_picklefile=f"mp_{vp}_{block}_{condition}",
            adam_steps=6000,
            lbfgs_steps=0,
        )
