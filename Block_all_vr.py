﻿import viz
import vizproximity
import vizinfo
import viztask
import random
import arm
import sys
import steamvr
import vizact
import os
import vizshape
########## NEW (start) ##########
vr_version = False
########## NEW (end) ##########
print (vr_version)
# Fenster auf angenehme Größe einstellen:
#viz.window.setSize(1900, 800)
#viz.fov(90)
########## NEW (start) ##########
if vr_version:
    hmd = steamvr.HMD()
    # schließe System, falls HMD nicht angeschlossen ist
    if not hmd.getSensor():
        sys.exit('SteamVR HMD not detected') # informative Fehlermeldung
    # Links bestehen aus Source (Argument 1) und Destination (Argument 2)
    # Bewegungen und Rotation werden von Source auf Destination übertragen
    viz.link(hmd.getSensor(), viz.MainView)
## Funktion, welche sich zw. Source und Destination schaltet
#def copy_rotation(source, destination):
#    """verstärke den Rotations-Input"""
#    
#    # fragt momentane Rotation von Source ab
#    yaw, pitch, roll = source.getEuler()
#    # speist sie verstärkt bei Destination ein
#    destination.setEuler(2*yaw+45, 2*pitch+90, 2*roll)
    
def control_arm(controller_1, controller_2, a, z, joint_1=arm.upper.joint, joint_2=arm.lower.joint):
#    yaw, pitch, roll = controller_1.getEuler()
#    x1, y1, z1, a1 = controller_1.getAxisAngle()
#    x2, y2, z2, a2 = controller_2.getAxisAngle()
#    arm.upper.joint.setAxisAngle(x1, y1, z1, a1)
#    arm.lower.joint.setAxisAngle(x2, y2, z2, a2)
    
    yaw1, pitch1, roll1 = controller_1.getEuler(viz.REL_LOCAL)
    yaw2, pitch2, roll2 = controller_2.getEuler(viz.REL_LOCAL)
    joint_1.setEuler(a*yaw1+45, a*pitch1+90, a*roll1)
    joint_2.setEuler(0, 0, z*roll2)
    
#    print(controller_1.getAxisAngle())
#    #print(controller_2.getEuler())
#    arm.upper.joint.setEuler(2*yaw+45, 2*pitch+90, 2*roll)
#    yaw, pitch, roll = controller_2.getEuler()
#    arm.lower.joint.setEuler(2*yaw+45, 2*pitch+90, 2*roll)
# Umgebung mit Anti-aliasing starten:
viz.setMultiSample(8)
viz.go()
# Lade Dojo Szene:
dojo = viz.addChild('dojo.osgb')
#Get the tracking data.
def getData(tracking_data, controller_tracking):
    orientation_upper = arm.upper.joint.getEuler()
    orientation_lower = arm.lower.joint.getEuler()
    orientation_hmd = viz.MainView.getEuler()
    #controller = steamvr.getControllerList()[0]
    orientation_controller = viz.MainView.getEuler()
    
    #Make a string out of the data.
    #data = str(orientation) + '\t' + str(position) + '\n'
    data = "\t".join(map(str,orientation_lower)) + "\t" + "\t".join(map(str,orientation_upper)) + "\n"
    input_data = "\t".join(map(str,orientation_controller)) + "\t" + "\t".join(map(str,orientation_hmd)) + "\n"
    #Write it to the tracking file.
    tracking_data.write(data)
    controller_tracking.write(input_data)
def participantInfo():
    """
    Diese Generator-Funktion ruft ein Fenster auf, in dem ein
    Pseudonym der Versuchsperson eingegeben werden kann.
    """
    # Add an InfoPanel with a title bar
    participantInfo = vizinfo.InfoPanel(
        "", title="Participant Information", align=viz.ALIGN_CENTER, icon=False
    )
    # Add name and ID fields
    textbox_id = participantInfo.addLabelItem("ID", viz.addTextbox())
    participantInfo.addSeparator(padding=(20, 20))
    # Add submit button aligned to the right and wait until it's pressed
    submitButton = participantInfo.addItem(
        viz.addButtonLabel("Submit"), align=viz.ALIGN_RIGHT_CENTER
    )
    yield viztask.waitButtonUp(submitButton)
    # Collect and return participant data
    id_str = textbox_id.get()
    participantInfo.remove()
    viztask.returnValue(id_str)
def main_experiment(N, participant, block, spheres):
    """
    Dieser Generator ist beinahe eine Kopie von `generate_data_string`
    aus dem Notebook ´Lösungsvorschlag.ipynb´. Es unterscheidet dadurch,
    dass die bisher zufälligen Zeiten durch gemessene Zeiten ersetzt
    werden.
    Das Argument N bestimmt wieviele Trials generiert werden sollen.
    """
    data = []
    for i in range(N):  #  für N Durchgänge...
        cond = random.randint(0, 6)
        print(cond)
        t = yield condition(start_ball=spheres["start_sphere"], goal_ball=spheres[cond], N=N, i=i, participant=participant, block=block, cond=cond, spheres=spheres)
        data = data + [[i, cond, t]]
    info.setText("Herzlichen Glückwunsch. Sie sind fertig!")
    viztask.returnValue(data)
def start_trial(start_ball):
    yield vizproximity.waitEnter(start_ball.sensor)
    # an dieser stelle könnte fadeTo(viz.YELLOW, time=3) oder ähnlich aufgerufen werden
    wait_time_over = viztask.waitTime(0.5)
    pre_exit = vizproximity.waitExit(start_ball.sensor)
    d = yield viztask.waitAny([wait_time_over, pre_exit])
    if d.condition is wait_time_over:
        pass
    elif d.condition is pre_exit:
        # An dieser Stelle die Farbe des Balls wieder resetten
        info.setText("Sie haben den Startball zu früh verlassen. Versuchen Sie es erneut.")
        yield start_trial(start_ball)
def condition(start_ball, goal_ball, N, i, participant, block, cond, spheres):
    """
    Hier wird ein Trial generiert: Condition 1 falls der rechte Ball
    der Startball, und Condition 2, falls der linke Ball der Startball
    ist.
    """
    for key, sphere in spheres.items():
        sphere._show_sphere()
    
    info.setText("Halten Sie die Hand kurz auf dem roten Startball.")
    # yield vizproximity.waitEnter(start_ball.sensor)
    yield start_trial(start_ball)
    #info.setText("Halten Sie den Ball so lange auf dem roten Ball, bis ein weiterer Ball erscheint.")
    for key, sphere in spheres.items():
        sphere._hide_sphere()
    goal_ball._show_sphere()
    
    #tracking_data_content = tracking_data.read()
    
    # yield rename(tracking_data, i)
    if block == 1:
        tracking_data = open("data/{}/block_1_tracking_trial_{}_of_{}_condition_{}.txt".format(participant, i, N, cond), 'w')
        controller_tracking = open("data/{}/block_1_controller_tracking_trial_{}_of_{}_condition_{}.txt".format(participant, i, N, cond), 'w')
    if block == 2:
        tracking_data = open("data/{}/block_2_tracking_trial_{}_of_{}_condition_{}.txt".format(participant, i, N, cond), 'w')
        controller_tracking = open("data/{}/block_2_controller_tracking_trial_{}_of_{}_condition_{}.txt".format(participant, i, N, cond), 'w')
    if block == 3:
        tracking_data = open("data/{}/block_3_tracking_trial_{}_of_{}_condition_{}.txt".format(participant, i, N, cond), 'w')
        controller_tracking = open("data/{}/block_3_controller_tracking_trial_{}_of_{}_condition_{}.txt".format(participant, i, N, cond), 'w')
    if block == 4:
        tracking_data = open("data/{}/block_4_tracking_trial_{}_of_{}_condition_{}.txt".format(participant, i, N, cond), 'w')
        controller_tracking = open("data/{}/block_4_controller_tracking_trial_{}_of_{}_condition_{}.txt".format(participant, i, N, cond), 'w')
    writer = vizact.ontimer(.1, getData, tracking_data, controller_tracking)
    #yield vizproximity.waitEnter(start_ball.sensor)
    info.setText("Berühren Sie nun den grünen Ball.")
    #yield vizproximity.waitExit(start_ball.sensor)
    #start_ball._exit_mid_sphere()
    # hier wird die derzeitige Uhrzeit gespeichert um die Zeit zu messen
    #goal_ball.sphere.alpha(0.5)
    start_time = viz.tick()
    success = vizproximity.waitEnter(goal_ball.sensor)
    # das Ergebnis wird abgespeichert...
    d = yield viztask.waitAny([success])
    writer.remove()
    tracking_data.close()
    controller_tracking.close()
    # ... um darauf entsprechend zu reagieren.
    #if d.condition is failure:
        #info.setText("fail")
        #result = "miss"  # Versagen wird als Ergebnis festgehalten...
    if d.condition is success:
        result = round(viz.tick() - start_time, 4)  # oder die Zeit
        #info.setText("success: " + str(result))
    else:
        print ("Something wrong here: either success or failure")
    goal_ball.sphere.alpha(0.0)
    # Die Versuchsperson hat 1 Sekunden um sich das Ergebnis anzuschauen.
    yield viztask.waitTime(1)
    # und das Resultat wird an `main_experiment` weitergegeben.
    viztask.returnValue(result)
    
info_text_1 = (
        "Herzlich Willkommen"
        + "!\nIn diesem Experiment sollen Sie lernen, den künstlichen grauen Arm zu kontrollieren.\n"
        + "Die Steuerung des Arms wurde Ihnen ja bereits erklärt. Ihre Aufgabe besteht darin,\n"
        + "immer abwechselnd den Startball und einen der 7 Zielbälle mit der gelben Hand zu berühren.\n"
        + "Bevor der Zielball erscheint, müssen Sie die Hand eine halbe Sekunde im Startball halten.\n"
        + "Versuchen Sie möglichst schnell und genau zu sein.\n"
        + "Diese Aufgabe wird in 4 Blöcken durchgeführt. Zunächst folgt der 1. Block.\n"
        + "Geben Sie der Versuchsleitung Bescheid wenn Sie diesen Text durchgelesen haben!")
    
info_text_2 = (
        "Willkommen zum 2.Block"
        + "!\nSie werden dieselbe Aufgabe bearbeiten wie im 1. Block.\n"
        + "Geben Sie der Versuchsleitung Bescheid wenn Sie diesen Text durchgelesen haben!")
info_text_3 = (
        "Willkommen zum 3.Block"
        + "!\nSie werden dieselbe Aufgabe bearbeiten wie im 1. und 2. Block.\n"
        + "Beachten Sie, dass die Zielbälle diesmal anders angeordnet sind.\n"
        + "Geben Sie der Versuchsleitung Bescheid wenn Sie diesen Text durchgelesen haben!")
info_text_4 = (
        "Willkommen zum 4.Block"
        + "!\nSie werden dieselbe Aufgabe bearbeiten wie in den ersten drei Blöcken.\n"
        + "Beachten Sie, dass die Steuerung des virtuellen Arms nun verändert wurde.\n"
        + "Geben Sie der Versuchsleitung Bescheid wenn Sie diesen Text durchgelesen haben!")
        
def mapping(block):
    if block == 4:
        if vr_version:
            # Gruppen sind leere Knoten (Graphentheorie)
            # Knoten sind hier Punkte im 3D-Koordinatensystem
            elbow_controller = viz.addGroup() # fungiert als Parten für Ellebogen
            # greift auf den zuerst beim PC registrierten Controller zu
            controller = steamvr.getControllerList()[0] #if vr version einfügen
            vizact.ontimer(.01, control_arm, controller, viz.MainView, a = 2, z = 4)
            #controller = steamvr.HMD() #if vr version einfügen
            
            controller.model = controller.addModel()
            # erlaube Controller, durch das Sichtfeld geführt zu werden
            controller.model.disable(viz.INTERSECTION)
            viz.link(controller, controller.model)
            #ax = vizshape.addAxes(parent=controller.model)
    else:
        if vr_version:
            # Gruppen sind leere Knoten (Graphentheorie)
            # Knoten sind hier Punkte im 3D-Koordinatensystem
            elbow_controller = viz.addGroup() # fungiert als Parten für Ellebogen
            # greift auf den zuerst beim PC registrierten Controller zu
            controller = steamvr.getControllerList()[0] #if vr version einfügen
            i = viz.FOREVER
            writer = vizact.ontimer(.01, control_arm, viz.MainView, controller, a = 3, z = 1)
            
#            controller.model = controller.addModel()
#            # erlaube Controller, durch das Sichtfeld geführt zu werden
#            controller.model.disable(viz.INTERSECTION)
#            viz.link(controller, controller.model)
            return writer
#            # erstelle Modell für den Controller
#            controller.model = controller.addModel(parent=elbow_controller)
#            # erlaube Controller, durch das Sichtfeld geführt zu werden
#            controller.model.disable(viz.INTERSECTION)
#            viz.link(controller, controller.model)
#            link_elbow = viz.link(controller, arm.lower.joint)
#            # Source gibt nur Bewegungen auf der Y-Achse
#            link_elbow.setMask(viz.LINK_AXIS_Y)
#            # Ergebnis von Trial und Error, um "Controls" in VR zu verbessern
#            link_elbow.preEuler([0, 0, 90])
#            link_elbow.postEuler([0, 90, 0])
        
        
def experiment_1(participant, block, spheres):
    """
    Generator Funktion um gesamten experimentellen
    Ablauf festzulegen.
    """
    #print ("Warte auf Eingabe der VP-ID")
    #participant = yield participantInfo()
    writer = mapping(block)
    participant = participant
    
    info.visible(viz.ON)
    info.setText(info_text_1)
    yield viztask.waitKeyDown(" ")
    ########## NEW (start) ##########
    canvas.setPosition(-1, 2.2, 5)
    ########## NEW (end) ##########
    results = yield main_experiment(N=50, participant=participant, block=block, spheres=spheres)
    # write participant data to file
    f = open("data/" + participant + "/block_1_experiment_data.csv", "w")
    p_name = "Participant: " + participant + "\n\n"
    description = "Trial\tCondition\tTime\n"
    f.write(p_name + description)
    # write result of each trial
    for trial, condition, time in results:
        data = str(trial) + "\t\t" + str(condition) + "\t\t\t" + str(time) + "\n"
        f.write(data)
    f.close()
    #writer.remove()
def experiment_2(participant, block, spheres):
    """
    Generator Funktion um gesamten experimentellen
    Ablauf festzulegen.
    """
    writer = mapping(block)
    participant = participant
    
    info.visible(viz.ON)
    info.setText(info_text_2)
    yield viztask.waitKeyDown(" ")
    ########## NEW (start) ##########
    canvas.setPosition(-1, 2.2, 5)
    ########## NEW (end) ##########
    results = yield main_experiment(N=50, participant=participant, block=block, spheres=spheres)
    # write participant data to file
    f = open("data/" + participant + "/block_2_experiment_data.csv", "w")
    p_name = "Participant: " + participant + "\n\n"
    description = "Trial\tCondition\tTime\n"
    f.write(p_name + description)
    # write result of each trial
    for trial, condition, time in results:
        data = str(trial) + "\t\t" + str(condition) + "\t\t\t" + str(time) + "\n"
        f.write(data)
    f.close()
    #writer.remove()
def experiment_3(participant, block, spheres):
    """
    Generator Funktion um gesamten experimentellen
    Ablauf festzulegen.
    """
    writer = mapping(block)
    participant = participant
    info.visible(viz.ON)
    info.setText(info_text_3)
    yield viztask.waitKeyDown(" ")
    ########## NEW (start) ##########
    canvas.setPosition(-1, 2.2, 5)
    ########## NEW (end) ##########
    results = yield main_experiment(N=50, participant=participant, block=block, spheres=spheres)
    # write participant data to file
    f = open("data/" + participant + "/block_3_experiment_data.csv", "w")
    p_name = "Participant: " + participant + "\n\n"
    description = "Trial\tCondition\tTime\n"
    f.write(p_name + description)
    # write result of each trial
    for trial, condition, time in results:
        data = str(trial) + "\t\t" + str(condition) + "\t\t\t" + str(time) + "\n"
        f.write(data)
    f.close()
    #writer.remove()
def experiment_4(participant, block, spheres):
    """
    Generator Funktion um gesamten experimentellen
    Ablauf festzulegen.
    """
    mapping(block)
    participant = participant
    
    info.visible(viz.ON)
    info.setText(info_text_4)
    yield viztask.waitKeyDown(" ")
    ########## NEW (start) ##########
    canvas.setPosition(-1, 2.2, 5)
    ########## NEW (end) ##########
    results = yield main_experiment(N=50, participant=participant, block=block, spheres=spheres)
    
    # write participant data to file
    f = open("data/" + participant + "/block_4_experiment_data.csv", "w")
    p_name = "Participant: " + participant + "\n\n"
    description = "Trial\tCondition\tTime\n"
    f.write(p_name + description)
    # write result of each trial
    for trial, condition, time in results:
        data = str(trial) + "\t\t" + str(condition) + "\t\t\t" + str(time) + "\n"
        f.write(data)
    f.close()
# Hier wird eine Startposition der Kamera festgelegt, sodass man
# einen guten Überblick über das Geschehen hat.
#viz.MainView.setPosition(-0.65, 2.65, 0.14)
#viz.MainView.setEuler(23.13, 19.92, 1.71)
# Das Infopanel gibt der Versuchsperson Anweisungen.
# Es bleibt leer und unsichtbar am Anfang. Im Verlauf des Experiments
# wird es jedoch sichtbar gemacht und mit unterschiedlichem Text gefüllt.
########## NEW (start) ##########
canvas = viz.addGUICanvas()
# Argument 1: resolution = [width, height]
# Argument 2: size = [width, height]
canvas.setRenderWorld([320, 180], [2, viz.AUTO_COMPUTE])
canvas.setPosition(-1, 3, 5)
info = vizinfo.InfoPanel("", parent=canvas, align=viz.ALIGN_CENTER_TOP)
########## NEW (end) ##########
info.visible(viz.OFF)
# Hier wird das Experiment gestartet, d.h. es springt von
# yield statement zu yield statement.
def master():
    print ("Warte auf Eingabe der VP-ID")
    participant = yield participantInfo()
    dn = "data/{}".format(participant)
    if os.path.exists(dn):
        print("Achtung, {} existiert bereits. Eventuell werden Daten ueberschrieben!".format(dn))
    else:
        os.makedirs(dn)
    import environment
    list_1 = [-90, -45, -20, 0, 0, 20, 45, 90]
    list_2 = [-87.4, -42.4, -20, 0, 2.6, 20, 47.6, 92.6]
    spheres_1 = environment.create_spheres(0.6, l=list_1)
    yield experiment_1(participant, block=1, spheres=spheres_1)
    yield viztask.waitKeyDown(" ")
    yield experiment_2(participant, block=2, spheres=spheres_1)
    yield viztask.waitKeyDown(" ")
    for i in range(7):
        spheres_1[i].sphere.alpha(0)
    spheres = environment.create_spheres(0.5, l=list_2)
    yield experiment_3(participant, block=3, spheres=spheres)
    yield viztask.waitKeyDown(" ")
    for i in range(7):
        spheres[i].sphere.alpha(0)
    for i in range(7):
        spheres_1[i].sphere.alpha(1)
    yield experiment_4(participant, block=4, spheres=spheres_1)
viztask.schedule(master())